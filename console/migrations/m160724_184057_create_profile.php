<?php

use yii\db\Migration;

/**
 * Handles the creation for table `profile`.
 */
class m160724_184057_create_profile extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('profile', [
            'id' => $this->primaryKey(),
            'name' => 'VARCHAR(45) NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('profile');
    }
}
