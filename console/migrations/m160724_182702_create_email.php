<?php

use yii\db\Migration;

/**
 * Handles the creation for table `email`.
 */
class m160724_182702_create_email extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('email', [
            'id' => $this->primaryKey(),
            'email' => 'VARCHAR(45) NOT NULL UNIQUE',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('email');
    }
}
