<?php

use yii\db\Migration;

/**
 * Handles the creation for table `payments`.
 */
class m160727_211356_create_payments extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('payments', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'count' => $this->integer(),
            'description' => $this->text(),
            'method' => $this->text(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11)
        ]);

        $this->createIndex('user_id', 'payments', 'user_id');

        $this->addForeignKey('payments_ibfk_1', 'payments', 'user_id', 'user', 'id', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('payments_ibfk_1', 'payments');

        $this->dropIndex('user_id', 'payments');

        $this->dropTable('payments');
    }
}
