<?php

use yii\db\Migration;

/**
 * Handles the creation for table `status`.
 */
class m160724_183501_create_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status', [
            'id' => $this->primaryKey(),
            'status_name' => 'VARCHAR(45) NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status');
    }
}
