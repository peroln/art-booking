<?php

use yii\db\Migration;

class m160724_202153_fk_address_city extends Migration
{
    public function safeUp()
    {
        // creates index for column `city_id`
        $this->createIndex(
            'address_city_idx',
            'address',
            'city_id'
        );
        // add foreign key for table `address`
        $this->addForeignKey(
            'fk_address_city1',
            'address',
            'city_id',
            'city',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        // drop foreign key for table `address`
        $this->dropForeignKey(
            'fk_address_city1',
            'address'
        );
        // drops index for column `city_id`
        $this->dropIndex(
            'address_city_idx',
            'address'
        );
    }
}
