<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_profile`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `profile`
 */
class m160724_185324_create_junction_user_and_profile extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_profile', [
            'user_id' => $this->integer(),
            'profile_id' => $this->integer(),
            'PRIMARY KEY(user_id, profile_id)',
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_profile-user_id',
            'user_profile',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_profile-user_id',
            'user_profile',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `profile_id`
        $this->createIndex(
            'idx-user_profile-profile_id',
            'user_profile',
            'profile_id'
        );

        // add foreign key for table `profile`
        $this->addForeignKey(
            'fk-user_profile-profile_id',
            'user_profile',
            'profile_id',
            'profile',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_profile-user_id',
            'user_profile'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_profile-user_id',
            'user_profile'
        );

        // drops foreign key for table `profile`
        $this->dropForeignKey(
            'fk-user_profile-profile_id',
            'user_profile'
        );

        // drops index for column `profile_id`
        $this->dropIndex(
            'idx-user_profile-profile_id',
            'user_profile'
        );

        $this->dropTable('user_profile');
    }
}
