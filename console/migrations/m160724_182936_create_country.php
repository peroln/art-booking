<?php

use yii\db\Migration;

/**
 * Handles the creation for table `country`.
 */
class m160724_182936_create_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('country', [
            'id' => $this->primaryKey(),
            'country_name' => 'VARCHAR(45) NULL',


        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('country');
    }
}
