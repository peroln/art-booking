<?php

use yii\db\Migration;

class m160724_202554_fk_address_email extends Migration
{

    public function safeUp()
    {
        // creates index for column `email_id`
        $this->createIndex(
            'address_email_idx',
            'address',
            'email_id'
        );
        // add foreign key for table `address`
        $this->addForeignKey(
            'fk_address_email1',
            'address',
            'email_id',
            'email',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        // drop foreign key for table `address`
        $this->dropForeignKey(
            'fk_address_email1',
            'address'
        );
        // drops index for column `email_id`
        $this->dropIndex(
            'address_email_idx',
            'address'
        );
    }
}
