<?php

use yii\db\Migration;

/**
 * Handles the creation for table `category`.
 */
class m160724_183918_create_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => 'VARCHAR(45) NULL',
            'profile_id' => 'INT NOT NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('category');
    }
}
