<?php

use yii\db\Migration;

class m160724_190900_fk_user_status extends Migration
{
    public function safeUp()
    {
        // creates index for column `status_id`
        $this->createIndex(
            'user_status_idx',
            'user',
            'status_id'
        );
        // add foreign key for table `user`
        $this->addForeignKey(
            'fk_user_status1',
            'user',
            'status_id',
            'status',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        // drop foreign key for table `user`
        $this->dropForeignKey(
            'fk_user_status1',
            'user'
        );
        // drop index for column `status`
        $this->dropIndex(
            'user_status_idx',
            'user'
        );
    }
}
