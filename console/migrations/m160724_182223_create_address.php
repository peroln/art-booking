<?php

use yii\db\Migration;

/**
 * Handles the creation for table `address`.
 */
class m160724_182223_create_address extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('address', [
            'id' => $this->primaryKey(),
            'email_id' => 'INT NOT NULL',
            'country_id' => 'INT NOT NULL',
            'city_id' => 'INT NOT NULL'

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('address');
    }
}
