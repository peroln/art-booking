<?php

use common\components\helpers\migration;

/**
 * Handles the creation for table `main_profile_info`.
 */
class m160727_191602_create_main_profile_info extends migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('main_profile_info', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(11),
            'title' => $this->text(),
            'short_description' => $this->text(),
            'full_description' => $this->text(),
            'specialization' => $this->text(),
            'composition' => $this->integer(11),
            'action_time' => $this->integer(11),
            'fee' => $this->integer(11),
            'comment_to_fee' => $this->text(),
            'country_tour' => $this->enum(['0','1']),
            'abroad_tour' => $this->enum(['0','1']),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ]);

        $this->createIndex('category_id', 'main_profile_info', 'category_id');

        $this->addForeignKey('main_profile_info_ibfk_1', 'main_profile_info', 'category_id', 'category', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('main_profile_info_ibfk_1', 'main_profile_info');

        $this->dropIndex('category_id', 'main_profile_info');

        $this->dropTable('main_profile_info');
    }
}
