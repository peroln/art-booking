<?php

use yii\db\Migration;

class m160724_200803_fk_address_country extends Migration
{
    public function safeUp()
    {
        // creates index for column `country_id`
        $this->createIndex(
            'address_country_idx',
            'address',
            'country_id'
        );
        // add foreign key for table `address`
        $this->addForeignKey(
            'fk_address_country1',
            'address',
            'country_id',
            'country',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        // drop foreign key for table `address`
        $this->dropForeignKey(
            'fk_address_country1',
            'address'
        );
        // drops index for column `country_id`
        $this->dropIndex(
            'address_country_idx',
            'address'
        );
    }
}
