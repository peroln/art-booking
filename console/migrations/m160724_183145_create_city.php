<?php

use yii\db\Migration;

/**
 * Handles the creation for table `city`.
 */
class m160724_183145_create_city extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'city_name' => 'VARCHAR(45) NULL',
            'country_id' => 'INT NOT NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('city');
    }
}
