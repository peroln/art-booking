<?php

use yii\db\Migration;

class m160724_193822_fk_user_address extends Migration
{
    public function safeUp()
    {
        // creates index for column `address_id`
        $this->createIndex(
            'user_address_idx',
            'user',
            'address_id'
        );
        // add foreign key for table `user`
        $this->addForeignKey(
            'fk_user_address1',
            'user',
            'address_id',
            'address',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        // drop foreign key for table `user`
        $this->dropForeignKey(
            'fk_user_address1',
            'user'
        );
        // drops index for column `address_id`
        $this->dropIndex(
            'user_address_idx',
            'user'
        );
    }
}
