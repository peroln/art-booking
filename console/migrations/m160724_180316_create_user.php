<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user`.
 */
class m160724_180316_create_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'first_name' => 'VARCHAR(45) NOT NULL',
            'middle_name' => 'VARCHAR(45) NULL',
            'last_name' => 'VARCHAR(45) NOT NULL',
            'status_id' => 'INT  NOT NULL',
            'address_id' => 'INT NOT NULL',
            'password_hash' => 'VARCHAR(45) NOT NULL',
            'auth_key' => 'VARCHAR(45) NULL',
            'password_reset_token' => 'VARCHAR(45) NULL',
            'role' => 'INT UNSIGNED NOT NULL DEFAULT "1"',
            'created' => 'INT UNSIGNED NOT NULL',
            'updated' => 'INT NULL',



        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
