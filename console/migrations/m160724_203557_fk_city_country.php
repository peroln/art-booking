<?php

use yii\db\Migration;

class m160724_203557_fk_city_country extends Migration
{

    public function safeUp()
    {
        // creates index for column `country_id`
        $this->createIndex(
            'city_country_idx',
            'city',
            'country_id'
        );
        // add foreign key for table `city`
        $this->addForeignKey(
            'fk_city_country1',
            'city',
            'country_id',
            'country',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        // drop foreign key for table `city`
        $this->dropForeignKey(
            'fk_city_country1',
            'city'
        );
        // drops index for column `country_id`
        $this->dropIndex(
            'city_country_idx',
            'city'
        );
    }
}
