<?php

use yii\db\Migration;

class m160724_204538_fk_category_profile extends Migration
{

    public function safeUp()
    {
        // creates index for column `profile_id`
        $this->createIndex(
            'category_profile_idx',
            'category',
            'profile_id'
        );
        // add foreign key for table `category`
        $this->addForeignKey(
            'fk_category_profile1',
            'category',
            'profile_id',
            'profile',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        // drop foreign key for table `category`
        $this->dropForeignKey(
            'fk_category_profile1',
            'category'
        );
        // drops index for column `profile_id`
        $this->dropIndex(
            'category_profile_idx',
            'category'
        );
    }
}
