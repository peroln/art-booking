<?php

use yii\db\Migration;

/**
 * Handles the creation for table `attachments`.
 */
class m160727_203654_create_attachments extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('attachments', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->text(),
            'type' => $this->text(),
            'path' => $this->text(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11)
        ]);

        $this->createIndex('user_id', 'attachments', 'user_id');

        $this->addForeignKey('attachments_ibfk_1', 'attachments', 'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('attachments_ibfk_1', 'attachments');

        $this->dropIndex('user_id', 'attachments');

        $this->dropTable('attachments');
    }
}
