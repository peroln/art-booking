<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "calendar".
 *
 * @property integer $id
 * @property integer $performer_id
 * @property integer $name_event_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Performer $performer
 * @property NameEvent $nameEvent
 */
class Calendar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calendar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['performer_id', 'name_event_id'], 'required'],
            [['performer_id', 'name_event_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name_event_id'], 'unique'],
            [['performer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Performer::className(), 'targetAttribute' => ['performer_id' => 'id']],
            [['name_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => NameEvent::className(), 'targetAttribute' => ['name_event_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'performer_id' => 'Performer ID',
            'name_event_id' => 'Name Event ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerformer()
    {
        return $this->hasOne(Performer::className(), ['id' => 'performer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNameEvent()
    {
        return $this->hasOne(NameEvent::className(), ['id' => 'name_event_id']);
    }
}
