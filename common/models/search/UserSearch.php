<?php
namespace common\models\search;

use common\models\Role;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 *
 * @property integer $pageSize
 * @property integer $pageCurrent
 */
class UserSearch extends User
{

    public $pageSize = 10;
    public $pageCurrent = 0;

    public $sort = [
        'defaultOrder' => [
            'created_at' => SORT_DESC,
//            'first_name' => SORT_ASC,
//            'middle_name' => SORT_ASC,
//            'last_name' => SORT_ASC,
//            'email' => SORT_ASC,
//            'birthday' => SORT_ASC,
//            'role' => SORT_ASC,
//            'gender' => SORT_ASC,
        ]
    ];
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pageSize', 'pageCurrent'], 'integer'],
            [['id',  'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['first_name','role_id', 'middle_name', 'last_name', 'email', 'birthday', 'gender', 'password_hash', 'password_reset_token', 'auth_key'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
                'page' => $this->pageCurrent,
            ],
            'sort' => $this->sort,
        ]);

        $query->joinWith('role0');
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'role.name', $this->role_id]);

        return $dataProvider;
    }
}
