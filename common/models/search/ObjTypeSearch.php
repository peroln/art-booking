<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ObjType;

/**
 * ObjTypeSearch represents the model behind the search form about `common\models\ObjType`.
 */
class ObjTypeSearch extends ObjType
{
    public $pageSize;
    public $pageCurrent;
    public $sort;
    public $dir;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'updated_by', 'updated_by', 'created_at', 'created_by' ], 'integer'],
            [['title', 'describe'], 'safe'],
            [['pageSize', 'pageCurrent', 'sort', 'dir' ], 'safe'],
        ];
    }

    /** 
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = ObjType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
                'page' => $this->pageCurrent,
            ],
            'sort' => [

                'defaultOrder' => [
                    'id' => SORT_ASC,
                    'title'=> SORT_ASC,
                    'status'=> SORT_ASC,
                    'updated_by'=> SORT_ASC,
                    'updated_at'=> SORT_ASC,
                    'created_at'=> SORT_ASC,
                    'created_by'=> SORT_ASC,
                ],
            ],
        ]);

        

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'describe', $this->describe]);

        return $dataProvider;
    }
}
