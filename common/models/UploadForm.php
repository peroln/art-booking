<?php
namespace common\models;

use common\components\File;
use yii\base\Model;
use yii\web\UploadedFile;
use yii;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $imageFiles;
    public $preview;
    public $files;


    public function rules()
    {
        return [
            [['imageFile'], 'file', 'extensions' => 'png, jpg, jpeg'],
            [['imageFiles'], 'file', 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 25, 'on' => 'fotos'],
            [['imageFiles'], 'file', 'extensions' => 'mp3, AAC, WAV, AIFF, APE, FLAC', 'maxFiles' => 25, 'on' => 'audio'],
            [['preview', 'files'], 'string'],
            [['files', 'files'], 'string'],
        ];
    }

    public function upload()
    {
       if ($this->validate()) {

            $this->imageFile->saveAs($this->preview . '/' . $this->imageFile->name);
            return  '/' . $this->imageFile->name;
        } else {

            return false;
        }
    }

    public function uploads()
    {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $file->saveAs($this->preview . '/' . $this->imageFiles->name);
            $res[] = '/' . $this->imageFiles->name;
            }
            return $res;
        } else {
            return false;
        }
    }

    /**
     * @return bool||string
     */
    public function errors()
    {
        foreach ($this->getErrors() as $error){
            return $error[0];
        }
        return false;
    }
}