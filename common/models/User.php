<?php

namespace common\models;

use common\components\helpers\ExtendedActiveRecord;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $email
 * @property integer $birthday
 * @property string $gender
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $role_id
 * @property integer $status
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Profile[] $profiles
 */
class User extends ExtendedActiveRecord implements IdentityInterface
{
    const ROLE_CUSTOMER = 1;
    const ROLE_PERFORMER = 2;
    const ROLE_ADMIN = 3;
    public $password;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'string', 'min' => 6],
            [['password_hash', 'email', 'role_id' ], 'required'],
            [['gender'], 'in', 'range' => ['male', 'female']],
            [['birthday'], 'filter', 'filter' => function($value){
                if($value === NULL){
                    $value = 0;
                }
                $value = is_int($value) ? $value : strtotime($value);
                return $value;
            }],
            [['birthday'], 'integer'],
            [['first_name', 'middle_name', 'last_name',], 'string', 'max' => 100],
            [['password_hash', 'password_reset_token', 'auth_key'], 'string', 'max' => 255],
            [['email', 'auth_key', ], 'unique', 'message' => 'This is not unique parametr.'],
            [ 'email', 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            ['email', 'email', 'message' => 'Please, choose normal email!.'],
            ['role_id', 'in', 'range' => [self::ROLE_CUSTOMER, self::ROLE_PERFORMER, self::ROLE_ADMIN]],
            ['status', 'in', 'range' => [ExtendedActiveRecord::STATUS_ACTIVE, ExtendedActiveRecord::STATUS_DELETED]],
            ['status', 'default', 'value' => ExtendedActiveRecord::STATUS_ACTIVE],
        ];

    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'birthday' => 'Birthday',
            'gender' => 'Gender',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'auth_key' => 'Auth Key',
            'role' => 'Role',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ]
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by'
            ]
        ];
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = \Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString(32);
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    /**
     * @param $model
     * @return array
     */
    public static function auth($auth_key){
        if($user = self::find()->where(['auth_key' => $auth_key])->one()) {
            return $user;
        }else{
            return false;
        }
    }
    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }
    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }



    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
//        if (!static::isPasswordResetTokenValid($token)) {
//            return null;
//        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param $user_id integer
     * @param $avatar string
     */
    public static function getAvatar($user_id){
        $query = new Query();
        $avatar = $query->select('thumbnail')->from('attachment')->where(['obj_id' => $user_id, 'obj_type' => 'avatar'])->one();
//var_dump($avatar); exit;
        if($avatar){
    $avatar = Yii::getAlias('@root').'/perol/'.$avatar['thumbnail'];
}else{
    $avatar = Yii::getAlias('@root').'/perol/static/web/v1/attachment/avatar/avatar_m/avatar_m.png';
}
        return $avatar;
    }

    /**
     * @param $user_id integer
     * @param $image string
     */
    public static function getImage($user_id){
        $query = new Query();
//        var_dump($user_id); exit;
        $image = $query->select('thumbnail')->from('attachment')->where(['obj_id' => $user_id, 'obj_type' => 'user'])->one();
//        var_dump($image); exit;
        if($image){
            $image = Yii::getAlias('@root').'/perol/'.$image['thumbnail'];
        }else{
            $image = Yii::getAlias('@root').'/perol/static/web/v1/attachment/user/image_m/default_m.png';
        }
        return $image;
    }

    public function getRole0()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @param $auth_key
     * @return array
     */
    public static function getUserId($auth_key){
        if($user = self::find()->where(['auth_key' => $auth_key])->one()) {
            return $user->id;
        }else{
            return false;
        }
    }
}
