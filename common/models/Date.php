<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "date".
 *
 * @property integer $name_event_id
 * @property integer $date
 *
 * @property NameEvent $nameEvent
 */
class Date extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'date';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_event_id', 'date'], 'required'],
            [['name_event_id', 'date'], 'integer'],
            [['name_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => NameEvent::className(), 'targetAttribute' => ['name_event_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name_event_id' => 'Name Event ID',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNameEvent()
    {
        return $this->hasOne(NameEvent::className(), ['id' => 'name_event_id']);
    }
}
