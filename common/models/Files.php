<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property string $path
 *
 * @property MainProfileInfo[] $mainProfileInfos
 */
class Files extends \common\components\helpers\ExtendedActiveRecord
{
    public $tempName;
    public $error;
    public $file;
    public $del_img;
    public $material_type;
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['path'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'png, jpg, txt, pdf'],
            [['del_img'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainProfileInfos()
    {
        return $this->hasMany(MainProfileInfo::className(), ['files_id' => 'id']);
    }
    
//    public function saveAs($path, $deleteTempFile = true)
//    {
//        if ($this->error == UPLOAD_ERR_OK) {
//            if ($deleteTempFile) {
//                return move_uploaded_file($this->tempName, $path);
//            } elseif (is_uploaded_file($this->tempName)) {
//                return copy($this->tempName, $path);
//            }
//        }
//        return false;
//    }
    public function createDirectory($path) {
        //$filename = "/folder/{$dirname}/";
        if (file_exists($path)) {
            //echo "The directory {$path} exists";
        } else {
            mkdir($path, 0775, true);
            //echo "The directory {$path} was successfully created.";
        }
    }
}
