<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "album".
 *
 * @property integer $id
 * @property string $name
 * @property string $preview
 * @property integer $profile_id
 * @property integer $album_id
 * @property integer $album_type
 * @property string $description
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Profile $profile
 */
class Album extends \common\components\helpers\ExtendedActiveRecord
{
    public $album_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'profile_id'], 'required'],
            [['album_type', 'album_id', 'profile_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['description', 'preview'], 'string'],
            [['name'], 'string', 'max' => 70],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'album_id' => 'Album ID',
            'name' => 'Name',
            'profile_id' => 'Profile ID',
            'description' => 'Description',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'album_type' => 'Album Type'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }


    public function getPreview(){
        if($this->preview){
             $this->preview = Yii::getAlias('@root').'/perol/'.$this->preview;
                return $this->preview;
        }
        return Yii::getAlias('@root').'/perol/static/web/v1/attachment/avatar/avatar_m/avatar_m.png';
    }

    /**
     * @return bool||string
     */
    public function errors()
    {
        foreach ($this->getErrors() as $error){
            return $error[0];
        }
        return false;
    }

}
