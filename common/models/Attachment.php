<?php

namespace common\models;

use common\components\File;
use common\components\Uploader;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\helpers\ExtendedActiveRecord;

/**
 * This is the model class for table "attachment".
 *
 * @property integer $id
 * @property string $obj_id
 * @property string $obj_type
 * @property string $name
 * @property string $new_name
 * @property string $type
 * @property string $path
 * @property string $thumbnail
 * @property integer $show
 * @property integer $music_style_id
 * @property integer $status
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Genre $Genre
 */
class Attachment extends ExtendedActiveRecord
{
    const PATH_MODULE = 'static/web/v1/attachment/';
    const TYPE_IMAGE = 0;
    const TYPE_VIDEO = 1;
    const TYPE_AUDIO = 2;
    const TYPE_FILE = 3;

    const OBJ_TYPE_USER = 'user';
    const OBJ_TYPE_PROFILE = 'profile';
    const OBJ_TYPE_EVENT = 'event';

    public $new_name;
    public $user_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attachment';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['obj_id', 'type', 'show', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['obj_id',], 'required'],
            [['obj_type'], 'string'],
            [['name'], 'string', 'max' => 100],
            [['path', 'thumbnail'], 'string', 'max' => 255],
            ['status', 'default', 'value' => 1],
        ];
    }

    /**
     * @param $obj_id
     * @param $obj_type
     * @param int $hide
     * @return bool|int
     * @throws \yii\base\Exception
     */

    public static function UploadFiles($obj_id, $obj_type, $hide = 0)
    {
        /**
         * @var $files File[]
         */

        $files = Uploader::loadFiles();

        foreach ($files as $file) {
            /**
             *  creating paths and directories
             */
            $path = Yii::getAlias('@' . File::PATH_MODULE) . $obj_type . '/' . $obj_id . '/' . $file->extension;
//            $thumbnailPath = Yii::getAlias('@' . File::PATH_MODULE) . $obj_type . '/' . $obj_id . '/' . $file->extension . '/' . 'thumbnail';
//            FileHelper::createDirectory($thumbnailPath, 0777);
            FileHelper::createDirectory($path, 0777);

            /**
             * creating unique name for file and save it
             */
            $new_name = uniqid();
            $bool = $file->saveAs($path . '/' . $file->baseName . '_' . $new_name . '.' . $file->extension);

            /**
             * write data into database
             */
            if ($bool) {
                $model = new Attachment();

                $filename = pathinfo($file->name, PATHINFO_EXTENSION);
                $model->name = $file->baseName . $new_name;
                $model->obj_type = $obj_type;
                $model->obj_id = $obj_id;
                $url = File::PATH_MODULE . $obj_type . '/' . $obj_id . '/' . $file->baseName . $new_name . '.' . $file->extension;

                $model->path = $url;

                if ($filename == 'jpg' || $filename == 'jpeg' || $filename == 'gif' || $filename == 'png') {
                    $model->type = $obj_type;
                    $model->show = $hide;

                    $imagick = new \Imagick(realpath($path . '/' . $file->baseName . '_' . $new_name . '.' . $file->extension));
                    $imagick->thumbnailImage(100, 100, true, true);
//                    file_put_contents($thumbnailPath . '/' . $file->baseName . '_' . $new_name . '.' . $file->extension, $imagick);

                    $model->thumbnail = $path . '/' . $file->pathThumbnail;

                } elseif ($filename == 'avi' || $filename == 'mp4') {
                    $model->type = self::TYPE_VIDEO;
                } elseif ($filename == 'mp3') {
                    $model->type = self::TYPE_AUDIO;
                } else {
                    $model->type = self::TYPE_FILE;
                }

                return $model->save() ? $model->id : false;
            }
        }

        return false;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'genre_id' => 'Genre ID',
            'obj_id' => 'Obj ID',
            'obj_type' => 'Obj Type',
            'name' => 'Name',
            'type' => 'Type',
            'path' => 'Path',

            'thumbnail' => 'Thumbnail',
            'show' => 'Show',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenre()
    {
        return $this->hasOne(Genre::className(), ['id' => 'genre_id']);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    public static function getUrl($obj_id, $obj_type, $show)
    {
        $request = Attachment::find()->where([
            'obj_id' => $obj_id,
            'obj_type' => $obj_type,
            'show' => $show])
            ->one();

        if($request){
            return  $request->path;
        }
        return '';
    }

    public function getPath()
    {
        return $this->path;
    }


    public static function uploadBase64($user_id, $input_names)
    {

        foreach ($input_names as $input_name) {
            if (!(strpos($input_name, 'image') === false)) {
                $mid_path = 'user';

            }
            if (!(strpos($input_name, 'avatar') === false)) {
                $mid_path = 'avatar';
            }

            if (isset($mid_path)) {
                $path[] = Uploader::loadFilesBase64($input_name, $mid_path);
            } else {
                return false;
            }
        }

        if (isset($path)) {
            if ($path[0] || $path[1]) {

                if ($model_old = self::find()->where(['obj_id' => $user_id, 'obj_type' => $mid_path])->one()) {
                    if ($path[0] !== 0){
                        $old_path[0] = $model_old->path;
                    }
                    if (isset($path[1]) && ($path[1] !== 0)) {
                        $old_path[1] = $model_old->thumbnail;
                    }
                }


                if(!$model_old) {
                    $model = new self;
                }else{
                    $model = $model_old;
                }
//var_dump($model); exit;
                if ($model->load(['Attachment' => Yii::$app->request->post()])) {
                    $model->obj_id = $user_id;
                    $model->obj_type = $mid_path;
                    if ($path[0] !== 0) {
                        $model->path = $path[0];
                    }


                    if (($path[1] !== 0)) {
                        $model->thumbnail = $path[1];
                    }

                    $model->name = 'foto';
                    $model->status = 1;
                    $model->show = 1;


                    if ($model->save()) {
                        if (isset($old_path)) {

                            Uploader::deleteFiles($old_path);
                        }
                        return $model->path;
                    }
                }
                return ['errors' => $model->errors()];
            }
        }
        return false;
    }


}
