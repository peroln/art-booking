<?php
namespace common\models\site;

use common\models\Address;
use common\models\Email;
use Yii;
use common\components\helpers\ExtendedModel;
use common\models\User;

/**
 * Login form
 */
class LoginForm extends ExtendedModel
{
    public $first_name;
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // first_name and password are both required
            [[ 'password', 'email'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
//            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect email or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[first_rname]]
     *
     * @return User|null
     */
    protected function getUserToEmail()
    {
        $email_id = Email::findByEmail($this)->id;

        $address_id = Address::findOne(['email_id' => $email_id])->id;

        return User::findOne(['address_id' => $address_id]);
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
           $this->_user = User::findByEmail($this->email);
            return  $this->_user;
        }
    }
}