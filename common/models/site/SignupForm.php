<?php
namespace common\models\site;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $first_name;
    public $email;
    public $password;
    public $last_name;
    public $gender;
    public $birthday;
    public $middle_name;
    public $role_id;




    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'role_id'], 'required'],
            [['gender'], 'in', 'range' => ['male', 'female']],
            [['role_id'], 'in', 'range' => [User::ROLE_ADMIN, User::ROLE_PERFORMER, User::ROLE_CUSTOMER]],
            [['birthday', 'role_id'], 'integer'],
            [['first_name', 'middle_name', 'last_name', 'gender'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 255],
            [ 'email', 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            ['email', 'email', 'message' => 'Please, choose normal email!.'],
            ['password', 'string', 'min' => 8],
        ];
    }

    /**
     * Signs user up.
     * @param role_id integer
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->first_name = $this->first_name;
        $user->middle_name = $this->middle_name;
        $user->last_name = $this->last_name;
        if(User::find()->where(['email' => $this->email])->one()){
            return 'Сорри, дружище, но такой email уже зарегистрирован!';
        }
        $user->email = $this->email;
        $user->role_id = $this->role_id;
        $user->gender = $this->gender;
        $user->birthday = $this->birthday;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save(false) ? $user : null;

    }


}
