<?php

namespace common\models;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\components\helpers\ExtendedActiveRecord;

use Yii;
use yii\data\SqlDataProvider;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $city_id
 * @property string $title
 * @property string $company_name
 * @property string $description
 * @property integer $status
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $description_short
 * @property string $name
 * @property Event[] $events
 * @property Massage[] $massages
 * @property Performer $performer
 * @property City $city
 * @property User $user
 * @property Performer[] $performers
 * @property Subscribe[] $subscribes
 * @property Subscribe[] $subscribes0
 * @property Profile[] $profiles
 * @property Profile[] $profileId1s
 */
class Profile extends ExtendedActiveRecord
{
    public $name;
    public $description_short;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'city_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['user_id'], 'required'],
            ['status', 'default', 'value' => 1],
            [['description', 'description_short'], 'string'],
            [['title', 'name', 'company_name'], 'string', 'max' => 255],

            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'city_id' => 'City ID',
            'title' => 'Title',
            'company_name' => 'Company Name',
            'description' => 'Description',
            'description_short' => 'Description Short',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by'
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['profile_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMassages()
    {
        return $this->hasMany(Massage::className(), ['profile_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerformer()
    {
        return $this->hasOne(Performer::className(), ['id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerformers()
    {
        return $this->hasMany(Performer::className(), ['id' => 'performer_id'])->viaTable('rating', ['profile_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribes()
    {
        return $this->hasMany(Subscribe::className(), ['profile_id1' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribes0()
    {
        return $this->hasMany(Subscribe::className(), ['profile_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['id' => 'profile_id'])->viaTable('subscribe', ['profile_id1' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileId1s()
    {
        return $this->hasMany(Profile::className(), ['id' => 'profile_id1'])->viaTable('subscribe', ['profile_id' => 'id']);
    }

    public static function profilesByCity($id)
    {
        $provider = new SqlDataProvider([
            'sql' => 'SELECT * FROM profile WHERE city_id=:city_id',
            'params' => ['city_id' => $id],
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'title',
                    'view_count',
                    'created_at',
                ],
            ],
        ]);
        return $provider;
    }
    
}
