<?php

namespace common\models;

use yii\data\SqlDataProvider;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\components\helpers\ExtendedActiveRecord;
use Yii;

/**
 * This is the model class for table "performer".
 *
 * @property integer $id
 * @property integer $specialization_id
 * @property string $stage_name
 * @property integer $status
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Group[] $groups
 * @property Profile $id0
 * @property Specialization $specialization
 * @property PerformerGenre[] $performerGenres
 * @property Genre[] $Genres
 * @property Rating[] $ratings
 * @property Profile[] $profiles
 */
class Performer extends ExtendedActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'performer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'genre_id', 'status'], 'required'],
            ['status', 'default', 'value' => 1],
            [['id', 'specialization_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['stage_name'], 'string', 'max' => 100],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['id' => 'id']],
//            [['specialization_id'], 'exist', 'skipOnError' => true, 'targetClass' => Specialization::className(), 'targetAttribute' => ['specialization_id' => 'id']],
        ];
    }

    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'specialization_id' => 'Specialization ID',
            'stage_name' => 'Stage Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by'
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['performer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Profile::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialization()
    {
        return $this->hasOne(Specialization::className(), ['id' => 'specialization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerformerGenres()
    {
        return $this->hasMany(PerformerGenre::className(), ['performer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenres()
    {
        return $this->hasMany(Genre::className(), ['id' => 'genre_id'])->viaTable('performer_genre', ['performer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatings()
    {
        return $this->hasMany(Rating::className(), ['performer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['id' => 'profile_id'])->viaTable('rating', ['performer_id' => 'id']);
    }

    public function getAlbum()
    {
        return $this->hasMany(Album::className(), ['performer id' => 'id']);
    }

    public static function Performers($genre_id)
    {
        $provider = new SqlDataProvider([
            'sql' => 'SELECT *
        FROM performer_genre
        LEFT JOIN performer ON performer.id=performer_genre.performer_id
        WHERE performer_genre.genre_id=:genre_id',
            'params' => [':genre_id' => $genre_id],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $provider;
    }
}
