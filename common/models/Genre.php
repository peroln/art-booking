<?php

namespace common\models;

use common\components\File;
use common\components\helpers\ExtendedActiveRecord;
use common\components\Uploader;
use Imagine\Image\Box;
//use Imagine\Image\ImageInterface;
use Imagine\Image\ImageInterface;
use Imagine\Imagick\Imagine;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "genre".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property string $image_path
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 */
class Genre extends ExtendedActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ]
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by'
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'genre';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status', 'image_path'], 'required'],
            [['status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['image_path'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'image_path' => 'Image Path',
        ];
    }

    //    public static function myupload(ActiveRecord $model)
    public static function myupload()
    {
        /**
         * @var $file File
         */

        $files = Uploader::loadFiles();
        if(!$files){
            return false;
        }
        $objType = strtolower('Genre');
        foreach ($files as $key => $file) {
            FileHelper::createDirectory(
                Yii::getAlias('@' . File::PATH_MODULE . $objType . '/' . $file->typeByExtension),
                0777);
            if ($file->saveAs($file->getPathUrl($objType))) {

                return $image_path = $file->path;
            }
            return NULL;
        }
    }



}