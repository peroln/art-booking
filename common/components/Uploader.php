<?php

namespace common\components;

use yii;
use yii\base\Object;

//use yii\helpers\FileHelper;
//use common\components\File;

//use yii\web\UploadedFile;

/**
 * Class Uploader
 * @package common\components
 *
 * @property array $_files
 */
class Uploader extends Object
{
    private static $_files;

    /**
     * @return array
     */
    public static function loadFiles()
    {
        self::$_files = [];
        if (isset($_FILES) && is_array($_FILES)) {
            foreach ($_FILES as $class => $info) {
                self::loadFilesRecursive($class, $info['name'], $info['tmp_name'], $info['type'], $info['size'], $info['error']);
            }
        }
        return self::$_files;
    }

    /**
     * @param string $key
     * @param array|string $names
     * @param array|string $tempNames
     * @param array|string $types
     * @param array|string $sizes
     * @param array|string $errors
     */
    private static function loadFilesRecursive($key, $names, $tempNames, $types, $sizes, $errors)
    {
        if (is_array($names)) {
            foreach ($names as $i => $name) {
                self::loadFilesRecursive($key . '[' . $i . ']', $name, $tempNames[$i], $types[$i], $sizes[$i], $errors[$i]);
            }
        } elseif ($errors !== UPLOAD_ERR_NO_FILE) {
            self::$_files[$key] = new File([
                'name' => $names,
                'tempName' => $tempNames,
                'type' => $types,
                'size' => $sizes,
                'error' => $errors,
            ]);
        }
    }

    /**
     * @return array
     * @param $input_name string
     */
    public static function loadFilesBase64($input_name, $mid_path)
    {
        $base64 = Yii::$app->request->post($input_name);


//        $base64 = str_replace(" ", "+" , $base64);

        if ($array = explode("base64,", $base64)) {
            if (array_key_exists(1, $array)) {
                $img = $array[1];

                $base64 = stristr($base64, ';', true);
                $extension = explode("/", $base64);
                $data = base64_decode($img);

                $filePath = Yii::getAlias('@' . File::PATH_MODULE . $mid_path . '/' . $input_name . '/' . date('YmdHis') . '.' . $extension[1]);
                $success = file_put_contents($filePath, $data);
                $filePath = File::PATH_MODULE . $mid_path . '/' . $input_name . '/' . basename($filePath);
                return $success ? $filePath : 0;
            }
        }
        return 0;


    }

    /**
     * @return bool
     * @param $path string
     */
    public static function deleteFiles($paths)
    {

        if (is_array($paths)) {
            foreach ($paths as $path) {
                if ($path === 0) {
                    return false;
                } elseif (file_exists(Yii::getAlias('@perol') . '/' . $path)) {

                    return unlink(Yii::getAlias('@perol') . '/' . $path) ? true : false;
                }
            }
        } elseif ($paths === 0) {
            return false;
        } elseif (file_exists(Yii::getAlias('@perol') . '/' . $paths)) {
if(unlink(Yii::getAlias('@perol') . '/' . $paths)){
    // delete dirrectory if empty
    $del_dir = str_replace(strrchr($paths, '/'), '', $paths);
    if(!glob(Yii::getAlias('@perol') . '/' .$del_dir.'/*')){
        rmdir(Yii::getAlias('@perol') . '/' .$del_dir);
    }

    return  true;
}

        }
        return false;

    }
}