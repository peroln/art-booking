<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@perol', dirname(dirname(__DIR__)));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('@static', dirname(dirname(__DIR__)) . '/static');
Yii::setAlias('@static-files', dirname(dirname(__DIR__)) . '/static/files');
Yii::setAlias('@root',  'http://firstteam.vt-host.co.ua/');
