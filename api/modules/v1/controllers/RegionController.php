<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Region;
use common\models\search\RegionSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RegionController implements the CRUD actions for Region model.
 */
class RegionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [
                'create',
                'update',
                'delete',
                'view',
                'index'
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],
                ],
            ],
        ];
        
        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'create' => ['POST'],
                'update' => ['PUT', 'POST'],
                'delete' => ['POST', 'DELETE'],
                'view' => ['GET'],
                'index' => ['GET'],
            ],
        ];
        return $behaviors;
    }

    /**
     * http://<HOST>/api/web/v1/region/index
     * @method: GET
     * @params: page - номер страницы, size - количество выводимых записей.
     * @return array
     */
    public function actionIndex()
    {
        return new ActiveDataProvider([
            'query' => Region::find()
        ]);
//        $searchModel = new RegionSearch();
//        if ($searchModel->load(['RegionSearch' => Yii::$app->request->get()]) && $searchModel->validate()) {
//            $dataProvider = $searchModel->search();
//
//            return [ArrayHelper::toArray($dataProvider->getModels(), [
//                'common\models\Region' => [
//                    'id',
//                    'name'
//                ]
//            ]),
//                'count' => $dataProvider->query->count(),
//                'currentPage' => $dataProvider->pagination->page,
//                'pagesCount' => $dataProvider->pagination->pageCount
//            ];
//        }
//        return ['errors' => $searchModel->errors()];
    }

    /**
     * http://<HOST>/api/web/v1/region/view
     * @method: GET
     * @param integer $id
     * @return array []
     */
    public function actionView($id)
    {
        return ArrayHelper::toArray($this->findModel($id), [
            'common\models\Region' => [
                'id',
                'name'
            ]
        ]);
    }

    /**
     * Creates a new Region model.
     *
     * http://<HOST>/api/web/v1/region/create
     * @method: POST
     * @return array
     */
    public function actionCreate()
    {
        $model = new Region();

        if ($model->load(['Region' => Yii::$app->request->post()]) && $model->save()) {
            return ['id' => $model->id];
        }
        return ['errors' => $model->errors()];
    }

    /**
     * Updates an existing Region model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * http://<HOST>/api/web/v1/region/update
     * @method: POST
     * @return array
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if ($model->load(['Region' => Yii::$app->request->post()]) && $model->save()) {
            return ArrayHelper::toArray($model, [
                'common\models\Region' => [
                    'id',
                    'name'
                ]
            ]);
        }
        return ['errors' => $model->errors()];
    }

    /**
     * http://<HOST>/api/web/v1/region/delete
     * @method:POST
     * @return bool|false|int
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        return $this->findModel(Yii::$app->request->post('id'))->delete();
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Region::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
