<?php

namespace api\modules\v1\controllers;

use common\components\File;
use common\components\Uploader;
use Yii;
use common\models\Genre;
use common\models\search\GenreSearch;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Attachment;

/**
 * GenreController implements the CRUD actions for Genre model.
 */
class GenreController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [
                'create',
                'update',
                'delete',
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],
                ]
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'create' => ['POST'],
                'update' => ['PUT', 'POST'],
                'delete' => ['POST'],
                'view' => ['GET'],
                'index' => ['GET'],
                'role' => ['POST'],
                'upload' => ['post']
            ],
        ];
        return $behaviors;
    }

    /**
     * Lists all Genre models.
     * @return mixed
     */
    public function actionIndex()
    {

        /**
         *
         */
        $searchModel = new GenreSearch();

        if ($searchModel->load(['GenreSearch' => Yii::$app->request->get()]) && $searchModel->validate()) {

            $dataProvider = $searchModel->search();

            return [
                'models' => ArrayHelper::toArray($models = $dataProvider->getModels(), [
                    'common\models\Genre' => [
                        'id',
                        'name',
                        'status',
                        'image_path' => function ($model) {
                            return Yii::getAlias('@root') . '/perol/' . $model->image_path;
                        },
                    ]
                ]),
                'count' => $dataProvider->query->count(),
                'currentPage' => $dataProvider->pagination->page,
                'pagesCount' => $dataProvider->pagination->pageCount
            ];
        }
        return ['errors' => $searchModel->errors()];
    }

    /**
     * Displays a single Genre model.
     * @param integer $id
     * @return mixed
     */

    public function actionView($id)
    {
        return ArrayHelper::toArray($this->findModel($id), [
            'common\models\User' => [
                'id',
                'name',
                'status',
                'image_path'
            ]
        ]);
    }

    /**
     * Creates a new Genre model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Genre();

        if ($model->load(['Genre' => Yii::$app->request->post()])) {
            if ($path_new = $model::myupload()) {
                $model->name = Yii::$app->request->post('name');
                $model->image_path = (File::PATH_MODULE . 'genre/' . $path_new);
                $model->status = 1;
                if ($model->save()) {
                    return $model->id;
                }
                return ['errors' => $model->errors()];
            }
            return 'Не загрузилась картинка';
        }
        return ['errors' => $model->errors()];
    }

    /**
     * Updates an existing Genre model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if ($model->load(['Genre' => Yii::$app->request->post()])) {
            if ($path_new = $model::myUpload()) {
                $model->image_path = (File::PATH_MODULE . '/genre/' . $path_new);
            }
            if ($model->save()) {
                return $model->id;
            }
            return ['errors' => $model->errors()];
        }

    }

    /**
     * Deletes an existing Genre model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        try {
            $model->delete(false);
            Uploader::deleteFiles($model->image_path);
        } catch (Exception $e) {
            return ['errors' => $e->getMessage()];
        }


        return true;

//        if(Yii::$app->request->post('id')){
//            return $this->findModel(Yii::$app->request->post('id'))->delete();
//
//        }else {
//            return 'нету ID удаляемого жанра';
//        }
    }

    /**
     * Finds the Genre model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Genre the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Genre::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
