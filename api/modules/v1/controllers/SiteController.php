<?php

namespace api\modules\v1\controllers;

use common\models\Attachment;
use common\models\Profile;
use common\models\Role;
use common\models\Genre;
use common\models\Category;
use common\models\City;

use common\models\UploadForm;
use yii;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use common\models\site\LoginForm;
use common\models\site\PasswordResetRequestForm;
use common\models\site\ResetPasswordForm;
use common\models\site\SignupForm;

/**
 * Site controller for the `v1` module
 */
class SiteController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [
                'reset-password',
                'base'
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'login',
                'sign-up',
                'request-reset-password',
                'reset-password',
                'vk',
                'facebook',
                'base',

            ],
            'rules' => [
                [
                    'actions' => [
                        'login',
                        'sign-up',
                        'request-reset-password',
                        'vk',
                        'facebook'
                    ],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => [
                        'reset-password',
                        'base'
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'login' => ['POST'],
                'sign-up' => ['POST'],
                'request-reset-password' => ['POST'],
                'reset-password' => ['POST'],
                'auth' => ['POST'],
                'performer' => ['GET'],
                'home' => ['GET'],
                'city' => ['GET'],
                'vk' => ['POST', 'GET'],
                'facebook' => ['POST'],
                'genre' => ['GET'],
                'base' => ['POST'],
            ],
        ];


        return $behaviors;
    }

    /**
     * Logs in a user.
     * Method POST
     * param 'email', 'password' - required
     *
     * @return array
     */


    public function actionLogin()
    {

        $model = new LoginForm();

        if ($model->load(['LoginForm' => Yii::$app->request->post()])) {
            if ($user = User::find()->where(['email' => Yii::$app->request->post('email')])->one()) {
                if (Yii::$app->getSecurity()->validatePassword(Yii::$app->request->post('password'), $user->password_hash)) {

                    return ['auth_key' => $user->auth_key, 'avatar_m' => User::getAvatar($user->id)];
                }
            }
        }
        return ['errors' => $model->errors()];
    }

    /**
     * Signs user up.
     * Method POST
     * param 'first_name', 'last_name', 'email', 'password' - required
     * 'middle_name' , 'city_id', 'tarif_id', 'role', 'phone', 'fax', 'icq', 'skype', 'privacy_id'
     *
     * @return mixed
     */
    public function actionSignUp()
    {
        $model = new SignupForm();

        if ($model->load(['SignupForm' => Yii::$app->request->post()])) {

            if ($user = $model->signup()) {
                if ($user === 'Сорри, дружище, но такой email уже зарегистрирован!') {
                    return $user;
                }

                return ['auth_key' => $user->auth_key, 'avatar_m' => User::getAvatar($user->id)];

            }
            return ['errors' => $user->errors()];
        }
        return false;
    }

    /**
     * Requests password reset.
     * Sends token on email
     * Method POST
     * param 'email'
     *
     * @return bool
     */

    public function actionRequestResetPassword()
    {

        if (Yii::$app->request->post('email')) {
            $model = new PasswordResetRequestForm();
            $model->email = Yii::$app->request->post('email');
            return $model->sendEmail();
        }
        return 'Не получилось!';
    }

    /**
     * Resets password.
     * Method POST
     * param 'token', 'password' (it'l be a new password)
     *
     * @return bool
     */
    public function actionResetPassword()
    {


        $token = Yii::$app->request->post('token');

        $model = new ResetPasswordForm($token);
        $model->password = Yii::$app->request->post('password');

        if ($model->validate() && $model->resetPassword()) {

            return true;
        }

        return false;
    }

    /**
     * Method GET
     * param 'access-token'
     * @return array
     */
    public function actionAuth()
    {
        if ($auth_key = Yii::$app->request->post('auth_key')) {
            $user = User::auth($auth_key);
            $user->birthday = date('Y-m-d', $user->birthday);
            return $user;

        }
        return 'Саня, Так дело не пойдет! Мне нужен параметр auth_key, присланный методом POST';
    }


    public function actionCity()
    {
        return City::find()->where(['country_id' => Yii::$app->request->get('country_id')])->select(['name', 'id'])->all();

    }


// <a href="http://oauth.vk.com/authorize?client_id=5576983&display=page&scope=friends,email&redirect_uri=http://firstteam.vt-host.co.ua/perol/api/web/site/vk&response_type=code&v=5.53" title="Зайти через ВКонтакте">Зайти через ВКонтакте</a>
    public function actionVk()
    {

        if ($req = Yii::$app->request->post('code')) {

            $ch = curl_init('https://oauth.vk.com/access_token?client_id=5576983&client_secret=yfLEXzoIeoJfPxSArN5M&code=' . $req . '&redirect_uri=http://firstteam.vt-host.co.ua/');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $new_user = curl_exec($ch); // выполняем запрос curl
            curl_close($ch);


            $new_user = substr($new_user, 1, -1);
            $new_user = $this->strToArray($new_user);
            if ($new_user['user_id'] != NULL) {

                $chn = curl_init('https://api.vk.com/method/users.get?fields=photo_id,email,sex,bdate&access_token=' . $new_user['access_token'] . '&scope=email&v=5.53');
                curl_setopt($chn, CURLOPT_RETURNTRANSFER, true);
                $new_info = curl_exec($chn); // выполняем запрос curl
                curl_close($chn);

                $new_info = str_replace('"response":', '', $new_info);
                $new_info = substr($new_info, 3, -3);
                $new_info = $this->strToArray($new_info);


                $new_info['email'] = !empty($new_user['email']) ? $new_user['email'] : NULL;

                if ($new_info['email'] == NULL) {
//                    $model = new SignupForm();
//                    $model->first_name = $new_info['first_name'];
//                    $model->last_name = $new_info['last_name'];
//                    $model->gender = $new_info['sex'] == '2' ? 'male' : 'female';
//                    $model->password = '12345678';
//                    $model->email = false;
                    return ['message' => 'Извините, но в виду отсутствия email регистрация через эту соцсеть невозможна!'];
                } elseif ($user = User::find()->where(['email' => $new_info['email']])->one()) {

                    return ['auth_key' => $user->auth_key, 'avatar_m' => User::getAvatar($user->id),];
                } elseif (!Yii::$app->request->post('role_id')) {
                    return ['login' => false];
                } else {

                    $user = new User();
                    $user->first_name = $new_info['first_name'];
                    $user->last_name = $new_info['last_name'];
                    $user->email = $new_info['email'];
                    $user->password = $user->setPassword('12345678');
                    $user->generateAuthKey();
                    $user->gender = $new_info['sex'] == '2' ? 'male' : 'female';
                    $user->role_id = Yii::$app->request->post('role_id');

                    if ($user->save()) {
                        return ['auth_key' => $user->auth_key, 'avatar_m' => User::getAvatar($user->id),];
                    }
                    return 'Не удалось сохранить в базу! Проверька, дружок, свой код!';
                }
//                return 'Ошибочка вышла. Проверька, свой код!';
            }
            return 'Отсутствует почему-то id user, поэтому-то и ошибочка вышла! Но, ты, не сдавайся! У тебя получится!';
        }
        return 'Привет, Дружище!!! Не пришел ответ с самой дебильной соцсети!!! А может, ты мне его не передал, не знаю. ';
    }


    public function actionFacebook()
    {

        if ($req = Yii::$app->request->post('token')) {

            $chr = curl_init('https://graph.facebook.com/me?access_token=' . $req . '&fields=first_name,last_name,email,birthday,gender&scope=email');
            curl_setopt($chr, CURLOPT_RETURNTRANSFER, true);
            $new_user = curl_exec($chr); // выполняем запрос curl
            curl_close($chr);

            $new_user = json_decode($new_user);

            $new_user->email = property_exists($new_user, 'email') ? $new_user->email : NULL;

            if ($new_user->email == NULL) {
//                $model = new SignupForm();
//                $model->first_name = $new_user->first_name;
//                $model->last_name = $new_user->last_name;
//                $model->gender = $new_user->gender == 'male' ? 'male' : 'female';
//                $model->password = '12345678';
//                $model->email = false;
//
                return ['message' => 'Извините, но в виду отсутствия email регистрация через эту соцсеть невозможна!'];
            }
            if ($user = User::find()->where(['email' => $new_user->email])->one()) {

                return ['auth_key' => $user->auth_key, 'avatar_m' => User::getAvatar($user->id),];
            } elseif (!Yii::$app->request->post('role_id')) {
                return ['login' => false];
            }

            $user = new User();
            $user->first_name = $new_user->first_name;
            $user->last_name = $new_user->last_name;
            $user->email = $new_user->email;
            $user->password = $user->setPassword('12345678');
            $user->generateAuthKey();
            $user->gender = $new_user->gender;
            $user->role_id = Yii::$app->request->post('role_id');
            if ($user->save(false)) {
                return ['auth_key' => $user->auth_key, 'avatar_m' => User::getAvatar($user->id),];
            } else {
                return 'Увы, мой друг, но вы в базу не попали, попробуйте еще разок!';
            }


        }
        return 'Вадим не пришел параметр Post token!!!';
    }


    public function actionGenre()
    {
        $genre = Genre::find()->where(['status' => 1])->select(['name', 'id', 'image_path'])->all();
        foreach ($genre as $genr) {
            $genr->image_path = Yii::getAlias('@root') . '/perol/' . $genr->image_path;
        }

        return $genre;
    }

    public function actionRole()
    {
        return Role::find()->select(['name', 'id'])->where('id<=2')->all();
    }

    public function strToArray($string)
    {

        $string = str_replace('"', '', $string);
        $array = explode(",", $string);
        foreach ($array as $n_user) {
            $n_u = explode(":", $n_user);
            $result [$n_u[0]] = $n_u[1];
        }
        return $result;
    }

    public function actionBase()
    {

        if (Yii::$app->request->post('image')) {
            Attachment::uploadBase64();
            return true;
        }
        return false;
    }
//TODO Вывод и сортировка информации о всех зарегистрированных пользователях
    /*
     * Вывод и сортировка информации о всех зарегистрированных пользователях с
     *  созданным профилем как артист, т.е. performer
     *  Всё делаем методом GET
     *  host/.../site/home
     *  для сортировки по дате создания необходимо прислать
     *  параметр date с значением отличным от нуль
     *
     * для выборки по городу необходимо прислать параметр
     * city_id с id выбранного города
     *
     *  для выборки по жанру необходимо прислать параметр
     * genre_id с id выбранного жанра
     *
     * для сортировки по рейтингу(по умолчанию) надо прислать:
     *  $field = 'middle_mark';
      *  $sort = false;
     *
     * для сортировки по дате создания надо прислать:
     *  'date_select' - выбранная дата
     *
     */
    public function actionHome()
    {
        $city_id = NULL;
        $genre_id = NULL;

        if ($city_id = Yii::$app->request->get('city_id')) {
            if (!City::find()->select('id')->where(['id' => $city_id])->one()) {
                return 'Нет такого id';
            }

        }
        if ($genre_id = Yii::$app->request->get('genre_id')) {
            if (!Genre::find()->select('id')->where(['id' => $genre_id])->one()) {
                return 'Нет такого id';
            }

        }
        if ($date_select = Yii::$app->request->get('date_select')) {
            $date_select = strtotime($date_select);
//            var_dump($date_select);exit;
        }
        if(!isset($date_select)){
            $date_select = 1;
    }
        $field = 'middle_mark';
        $sort = (Yii::$app->request->get('sort') === 'true') ? true : false;
        if (Yii::$app->request->get('field') === 'date') {
            $field = 'date';
        }
        $query = new yii\db\Query();
        $subQuery = (new yii\db\Query())->from('attachment')->where(['obj_type' => 'user']);
        $calendarQuery = (new yii\db\Query())->select(['calendar.performer_id as id'])
            ->from('calendar')
            ->leftJoin('name_event', 'name_event.id = calendar.name_event_id')
            ->leftJoin('date', 'date.name_event_id = name_event.id')->where(['date.date' => $date_select]);

        $my_rown = (new yii\db\Query())
            ->select(['thumbnail as image_m', 'performer.id as performer_id',
                'performer.created_at AS date', 'user.id AS id', 'performer.genre_id',
                'genre.name AS genre_name', 'city.name as city_name', 'stage_name', 'AVG(mark) AS middle_mark', 'count(mark) as count_deals'])
            ->from('performer')
            ->join('LEFT JOIN', 'deal', 'deal.performer_id = performer.id')
            ->join('LEFT JOIN', 'profile', 'profile.id = performer.id')
            ->join('LEFT JOIN', 'user', 'user.id = profile.user_id')
            ->leftJoin(['a' => $subQuery], 'a.obj_id = user.id')
            ->leftJoin('genre', 'genre.id = performer.genre_id')
            ->leftJoin('city', 'city.id = profile.city_id')
            ->groupBy('id')
            ->filterWhere(['city.id' => $city_id, 'genre.id' => $genre_id])
            ->andFilterWhere(['not in', 'performer.id', $calendarQuery]);//->filterWhere(['city.id' => $city_id, 'genre.id' => $genre_id])
        $my_row = $query->from(['u' => $my_rown])
            ->orderBy([$field => $sort ? SORT_ASC : SORT_DESC, 'count_deals' => SORT_DESC, 'u.id' => SORT_ASC])->all();


        foreach ($my_row as $key => $sub_row) {

            if (isset($my_row[$key]['image_m']) && $my_row[$key]['image_m'] !== null) {
                $my_row[$key]['image_m'] = Yii::getAlias('@root') . '/perol/' . $sub_row['image_m'];
            } else {
                $my_row[$key]['image_m'] = Yii::getAlias('@root') . '/perol/static/web/v1/attachment/user/image_m/default_m.png';
            }
        }

        return $my_row;
    }

    // TODO просмотр профиля артиста
    /* 
     *  метод GET
     *  host/.../site/performer
     *  'performer_id' - id артиста
     *  [['performer_id'], require]
     *  return:
     *  [
     * album
            audio[]
            fotos[]
            videos[]
            avatar_m
            birthday
            city_name
            company_name
            description
            description_short
            email
            facebook
            first_name
            gender
            genre_name
            google_plus
            id (profile_id)
            image_m
            last_name
            lastfm
            middle_name
            phone
            site
            twitter
            vk
            youtube
     * ]
     *
     */
    public function actionPerformer()
    {
        if ($performer_id = Yii::$app->request->get('performer_id')) {
            $performer = (new yii\db\Query())->from('performer')
                ->select(['first_name', 'middle_name', 'middle_name',
                    'last_name', 'email', 'birthday', 'gender', 'genre.name AS genre_name',
                    'phone', 'site', 'facebook', 'twitter', 'vk', 'google_plus', 'lastfm', 'youtube',
                    'city.name AS city_name', 'description', 'company_name', 'title', 'user.id AS user_id'
                ])
                ->leftJoin('profile', 'profile.id = performer.id')
                ->leftJoin('city', 'city.id = profile.city_id')
                ->leftJoin('user', 'user.id = profile.user_id')
                ->leftJoin('genre', 'genre.id = performer.genre_id')
                ->leftJoin('contact', 'contact.profile_id = profile.id')
                ->where(['performer.id' => $performer_id])
                ->one();
            if (!$performer) {
                return 'Увы. Такого артиста не существует!';
            }
            $calendar = (new yii\db\Query())->select(['name_event.description', 'calendar.id AS calendar_id', 'calendar.name_event_id',
                'date.date', 'date.id AS date_id'])->from('calendar')
                ->leftJoin('name_event', 'name_event.id = calendar.name_event_id')
                ->leftJoin('date', 'date.name_event_id = name_event.id')
                ->where(['performer_id' => $performer_id])->all();
            foreach ($calendar as $key => $value) {
                $calendar[$key]['date'] = date('Y-m-d', $calendar[$key]['date']);
            }
            $result = [
                'id' => $performer_id,
                'first_name' => $performer['first_name'],
                'middle_name' => $performer['middle_name'],
                'last_name' => $performer['last_name'],
                'email' => $performer['email'],
                'birthday' => date('Y-m-d', $performer['birthday']),
                'gender' => $performer['gender'],
                'avatar_m' => User::getAvatar($performer['user_id']),
                'image_m' => User::getImage($performer['user_id']),
            ];


            $result['city_name'] = $performer['city_name'];
            $result['description'] = $performer['description'];
            $result['company_name'] = $performer['company_name'];
            $result['description_short'] = $performer['title'];


            $result['genre_name'] = $performer['genre_name'];
            $result['contact']['phone'] = $performer['phone'];
            $result['contact']['site'] = $performer['site'];
            $result['contact']['facebook'] = $performer['facebook'];
            $result['contact']['twitter'] = $performer['twitter'];
            $result['contact']['vk'] = $performer['vk'];
            $result['contact']['google_plus'] = $performer['google_plus'];
            $result['contact']['lastfm'] = $performer['lastfm'];
            $result['contact']['youtube'] = $performer['youtube'];


            $albums = (new \yii\db\Query())->select(['album.id as id', 'album.album_type AS album_type', 'preview',
                'album.name', 'album.description', 'attachment.path as thumbnail', 'attachment.id AS attachment_id'])
                ->from('album')->leftJoin('attachment', 'attachment.album_id = album.id ')
                ->where(['album.profile_id' => $performer_id,])->all();

            foreach ($albums as $key => $album) {

                switch ($albums[$key]['album_type']) {
                    case 1 :
                        $type = 'fotos';
                        $result['album'][$type][$album['id']]['attachment'][] = ['path' => Yii::getAlias('@root') . '/perol/' . $album['thumbnail'], 'foto_id' => $album['attachment_id']];
                        break;
                    case 2 :
                        $type = 'videos';
                        $result['album'][$type][$albums[$key]['id']]['attachment'][] = ['path' => $albums[$key]['thumbnail'], 'video_id' => $albums[$key]['attachment_id']];
                        break;
                    case 3 :
                        $type = 'audio';
                        $result['album'][$type][$album['id']]['attachment'][] = ['path' => Yii::getAlias('@root') . '/perol/' . $album['thumbnail'], 'audio_id' => $album['attachment_id'], 'audio_name' => substr(strrchr($album['thumbnail'], '/'), 1)];
                        break;
                    default :
                        return 'нет такого типа альбомов!';
                }
                $result['album'][$type][$album['id']]['description'] = $album['description'];
                $result['album'][$type][$album['id']]['album_id'] = $album['id'];
                $result['album'][$type][$album['id']]['name'] = $album['name'];
                $result['album'][$type][$album['id']]['preview'] = $album['preview'] ? Yii::getAlias('@root') . '/perol/' . $album['preview'] : Yii::getAlias('@root') . '/perol/static/web/v1/attachment/video/dfvfdv/video_default.png';
            }
            if (isset($result['album']['fotos'])) {
                foreach ($result['album']['fotos'] as $key => $value) {
                    $end['album']['fotos'][] = $value;
                }
                $result['album']['fotos'] = $end['album']['fotos'];
            }
            if (isset($result['album']['videos'])) {
                foreach ($result['album']['videos'] as $key => $value) {
                    $end['album']['videos'][] = $value;
                }
                $result['album']['videos'] = $end['album']['videos'];
            }
            if (isset($result['album']['audio'])) {
                foreach ($result['album']['audio'] as $key => $value) {
                    $end['album']['audio'][] = $value;
                }
                $result['album']['audio'] = $end['album']['audio'];
            }
            $result['calendar'] = $calendar;
            return $result;
        }
        return 'Нет performer_id - параметра!';
    }
}