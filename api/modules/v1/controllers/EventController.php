<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Event;
use common\models\search\EventSearch;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;


/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [
                'create',
                'update',
                'delete',
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['customer'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'create' => ['POST'],
                'update' => ['PUT'],
                'delete' => ['DELETE'],
                'view' => ['GET'],
                'index' => ['GET'],
            ],
        ];
        return $behaviors;
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        if ($searchModel->load(['EventSearch' => Yii::$app->request->get()]) && $searchModel->validate()) {

            $dataProvider = $searchModel->search();

            return [
                'models' => ArrayHelper::toArray($dataProvider->getModels(), [
                    'common\models\Event' => [
                        'id',
                        'place_id',
                        'customer_id',
                        'album_id',
                        'name',
                        'description',
                        'start_date',
                        'end_date',
                        'status'
                    ]
                ]),
                'count' => $dataProvider->query->count(),
                'currentPage' => $dataProvider->pagination->page,
                'pagesCount' => $dataProvider->pagination->pageCount
            ];
        }
        return ['errors' => $searchModel->errors()];
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return ArrayHelper::toArray($this->findModel($id), [
            'common\models\Event' => [
                'id',
                'place_id',
                'customer_id',
                'album_id',
                'name',
                'description',
                'start_date',
                'end_date',
                'status'
            ]
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Event();

        if ($model->load(['Event' => Yii::$app->request->post()]) && $model->save()) {
            return ['id' => $model->id];
        }
        return ['errors' => $model->errors()];
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * http://<HOST>/api/web/v1/event/update
     * @method: POST
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if (Yii::$app->user->can('update', ['post' => $model->created_by])) {


            if ($model->load(['Event' => Yii::$app->request->post()]) && $model->save()) {
                return ArrayHelper::toArray($this->findModel(Yii::$app->request->post('id')), [
                    'common\models\Event' => [
                        'id',
                        'place_id',
                        'customer_id',
                        'album_id',
                        'name',
                        'description',
                        'start_date',
                        'end_date',
                        'status'
                    ]
                ]);
            }
            return ['errors' => $model->errors()];
        }
        throw new ForbiddenHttpException;
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @return bool|false|int
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if (Yii::$app->user->can('delete', ['post' => $model->created_by])) {
            return $model->delete();
        }
        throw new ForbiddenHttpException;
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
