<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Deal;
use common\models\search\DealSearch;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DealController implements the CRUD actions for Deal model.
 */
class DealController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [
                'create',
                'update',
                'delete',
                'view',
                'index'
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'create',
                'update',
                'delete',
                'view',
                'index'
            ],
            'rules' => [
                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                        'view',
                        'index'
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'create' => ['POST'],
                'update' => ['PUT', 'POST'],
                'delete' => ['POST', 'DELETE'],
                'view' => ['GET'],
                'index' => ['GET'],
            ],
        ];
        return $behaviors;
    }

    /**
     * http://<HOST>/api/web/v1/deal/index
     * @method: GET
     * @params: page - номер страницы, size - количество выводимых записей.
     * @return array
     */
    public function actionIndex()
    {
        $searchModel = new DealSearch();
        if ($searchModel->load(['DealSearch' => Yii::$app->request->get()]) && $searchModel->validate()) {

            $dataProvider = $searchModel->search();

            return ['models' => ArrayHelper::toArray($dataProvider->getModels(), [
                'common\models\Deal' => [
                    'id',
                    'customer_id' => function ($model) {
                        /** @var $model Deal */
                        return $model->customer->company_name;
                    },
                    'performer_id' => function ($model) {
                        /** @var $model Deal */
                        return $model->performer->stage_name;
                    },
                    'mark',
                    'comment',
                    'start_date',
                    'end_date',
                    'status'
                ]
            ]),
                'count' => $dataProvider->query->count(),
                'currentPage' => $dataProvider->pagination->page,
                'pagesCount' => $dataProvider->pagination->pageCount
            ];
        }
        return ['errors' => $searchModel->errors()];
    }

    /**
     * http://<HOST>/api/web/v1/deal/view
     * @method: GET
     * @param integer $id
     * @return array []
     */
    public function actionView($id)
    {
        return ArrayHelper::toArray($this->findModel($id), [
            'common\models\Deal' => [
                'id',
                'customer_id' => function ($model) {
                    /** @var $model Deal */
                    return $model->customer->company_name;
                },
                'performer_id' => function ($model) {
                    /** @var $model Deal */
                    return $model->performer->stage_name;
                },
                'mark',
                'comment',
                'start_date',
                'end_date',
                'status'
            ]
        ]);
    }

    /**
     * Creates a new Deal model.
     *
     * http://<HOST>/api/web/v1/deal/create
     * @method: POST
     * @return array
     */
    public function actionCreate()
    {
        $model = new Deal();

        if ($model->load(['Deal' => Yii::$app->request->post()]) && $model->save()) {
            return ['id' => $model->id];
        }
        return ['errors' => $model->errors()];
    }

    /**
     * Updates an existing Deal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * http://<HOST>/api/web/v1/deal/update
     * @method: POST
     * @return array
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if ($model->load(['Deal' => Yii::$app->request->post()]) && $model->save()) {
            return ArrayHelper::toArray($this->findModel(Yii::$app->request->post('id')), [
                'common\models\Deal' => [
                    'id',
                    'customer_id' => function ($model) {
                        /** @var $model Deal */
                        return $model->customer->company_name;
                    },
                    'performer_id' => function ($model) {
                        /** @var $model Deal */
                        return $model->performer->stage_name;
                    },
                    'mark',
                    'comment',
                    'start_date',
                    'end_date',
                    'status'
                ]
            ]);
        }
        return ['errors' => $model->errors()];
    }

    /**
     * http://<HOST>/api/web/v1/deal/delete
     * @method:POST
     * @return bool|false|int
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        return $this->findModel(Yii::$app->request->post('id'))->delete();
    }

    /**
     * Finds the Deal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Deal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Deal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
