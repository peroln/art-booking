<?php

namespace api\modules\v1\controllers;

use common\components\File;
use common\components\Uploader;
use Yii;
use common\models\Article;
use common\models\search\ArticleSearch;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [
                'create',
                'update',
                'delete',
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'create' => ['POST'],
                'update' => ['PUT', 'POST'],
                'delete' => ['POST', 'DELETE'],
                'view' => ['GET'],
                'index' => ['GET'],
            ],
        ];
        return $behaviors;
    }

    /**
     * http://<HOST>/api/web/v1/article/index
     * @method: GET
     * @params: page - номер страницы, size - количество выводимых записей.
     * @return array
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        if ($searchModel->load(['ArticleSearch' => Yii::$app->request->get()]) && $searchModel->validate()) {

            $dataProvider = $searchModel->search();
            $models = $dataProvider->getModels();

            return [
                'models' => ArrayHelper::toArray($models, [
                    'common\models\Article' => [
                        'id',
                        'user_id',
                        'title',
                        'text',
                        'image_file' => function ($model) {
                            /** @var $model Article */
                            if (isset($model->image_file)) {
                                return Yii::getAlias('@root') . '/perol/' . $model->image_file;
                            }
                            return 'http://img1.joyreactor.cc/pics/comment/full/%D1%81%D0%BF%D0%B0%D0%BD%D1%87-%D0%B1%D0%BE%D0%B1-%D0%BC%D1%83%D0%BB%D1%8C%D1%82%D0%B8%D0%BA%D0%B8-%D0%B3%D0%BE%D1%80%D0%BA%D0%B0-%D0%BA%D1%80%D0%B8%D0%BF%D0%BE%D1%82%D0%B0-1891013.jpeg';
                        },
                        'status'
                    ]
                ]),
                'count' => $dataProvider->query->count(),
                'currentPage' => $dataProvider->pagination->page,
                'pagesCount' => $dataProvider->pagination->pageCount
            ];
        }
        return ['errors' => $searchModel->errors()];
    }

    /**
     * http://<HOST>/api/web/v1/article/view
     * @method: GET
     * @param integer $id
     * @return array []
     */
    public function actionView($id)
    {
        return ArrayHelper::toArray($this->findModel($id), [
            'common\models\Article' => [
                'id',
                'user_id',
                'title',
                'text',
                'image_file' => function ($model) {
                    /** @var $model Article */
                    if (isset($model->image_file)) {
                        return Yii::getAlias('@root') . '/perol/' . $model->image_file;
                    }
                    return 'http://img1.joyreactor.cc/pics/comment/full/%D1%81%D0%BF%D0%B0%D0%BD%D1%87-%D0%B1%D0%BE%D0%B1-%D0%BC%D1%83%D0%BB%D1%8C%D1%82%D0%B8%D0%BA%D0%B8-%D0%B3%D0%BE%D1%80%D0%BA%D0%B0-%D0%BA%D1%80%D0%B8%D0%BF%D0%BE%D1%82%D0%B0-1891013.jpeg';
                },
                'status'
            ]
        ]);
    }

    /**
     * Creates a new Article model.
     *
     * http://<HOST>/api/web/v1/article/create
     * @method: POST
     * @return array
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(['Article' => Yii::$app->request->post()]) && $model->validate()) {
            if ($path_new = $model::myUpload()) {
                $model->image_file = ($path_new);
                if ($model->save()) {
                    return $model->id;
                }
                return ['errors' => $model->errors()];
            }
            $model->save();
            return ['id' => $model->id];
        }
        return ['errors' => $model->errors()];
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * http://<HOST>/api/web/v1/article/update
     * @method: POST
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if (Yii::$app->user->can('update', ['post' => $model->created_by])) {


            if ($model->load(['Article' => Yii::$app->request->post()])) {

                if ($path_new = $model::myUpload()) {
                    $model->image_file = ($path_new);
                }

                $model->save();

                return ArrayHelper::toArray($model, [
                    'common\models\Article' => [
                        'id',
                        'user_id',
                        'title',
                        'text',
                        'image_file' => function ($model) {
                            /** @var $model Article */
                            if (isset($model->image_file)) {
                                return Yii::getAlias('@root') . '/perol/' . $model->image_file;
                            }
                            return 'http://img1.joyreactor.cc/pics/comment/full/%D1%81%D0%BF%D0%B0%D0%BD%D1%87-%D0%B1%D0%BE%D0%B1-%D0%BC%D1%83%D0%BB%D1%8C%D1%82%D0%B8%D0%BA%D0%B8-%D0%B3%D0%BE%D1%80%D0%BA%D0%B0-%D0%BA%D1%80%D0%B8%D0%BF%D0%BE%D1%82%D0%B0-1891013.jpeg';
                        },
                        'status'
                    ]
                ]);
            }
            return ['errors' => $model->errors()];
        }
        throw new ForbiddenHttpException;
    }

    /**
     * http://<HOST>/api/web/v1/article/delete
     * @method:POST
     * @return bool
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if (Yii::$app->user->can('delete', ['post' => $model->created_by])) {

            if (isset($model->image_file)) {
                $model->delete(false);
                Uploader::deleteFiles($model->image_file);

            } else
                $model->delete(false);
            return true;
        }
        throw new ForbiddenHttpException;
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
