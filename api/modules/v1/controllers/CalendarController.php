<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Calendar;
use common\models\search\CalendarSearch;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CalendarController implements the CRUD actions for Calendar model.
 */
class CalendarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [
                'create',
                'update',
                'delete',
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'create',
                'update',
                'delete',
            ],
            'rules' => [

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['performer']
                ]
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'create' => ['POST'],
                'update' => ['PUT', 'POST'],
                'delete' => ['POST', 'DELETE'],
                'view' => ['GET'],
                'index' => ['GET'],
            ],
        ];
        return $behaviors;
    }

    /**
     * http://<HOST>/api/web/v1/calendar/index
     * @method: GET
     * @params: page - номер страницы, size - количество выводимых записей.
     * @return array
     */
    public function actionIndex()
    {
        $searchModel = new CalendarSearch();
        if ($searchModel->load(['CalendarSearch' => Yii::$app->request->get()]) && $searchModel->validate()) {

            $dataProvider = $searchModel->search();
            $models = $dataProvider->getModels();

            return [
                'models' => ArrayHelper::toArray($models, [
                    'common\models\Calendar' => [
                        'id',
                        'performer_id',
                        'start_date',
                        'end_date',
                        'description'
                    ]
                ]),
                'count' => $dataProvider->query->count(),
                'currentPage' => $dataProvider->pagination->page,
                'pagesCount' => $dataProvider->pagination->pageCount
            ];
        }
        return ['errors' => $searchModel->errors()];
    }

    /**
     * http://<HOST>/api/web/v1/calendar/view
     * @method: GET
     * @param integer $id
     * @return array []
     */
    public function actionView($id)
    {
        return ArrayHelper::toArray($this->findModel($id), [
            'common\models\Calendar' => [
                'id',
                'performer_id',
                'start_date',
                'end_date',
                'description'
            ]
        ]);
    }

    /**
     * Creates a new Calendar model.
     *
     * http://<HOST>/api/web/v1/calendar/create
     * @method: POST
     * @return array
     */
    public function actionCreate()
    {
        $model = new Calendar();

        if ($model->load(['Calendar' => Yii::$app->request->post()]) && $model->save()) {
            return ['id' => $model->id];
        }
        return ['errors' => $model->errors()];
    }

    /**
     * Updates an existing Calendar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * http://<HOST>/api/web/v1/calendar/update
     * @method: POST
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if (Yii::$app->user->can('update', ['post' => $model->created_by])) {

        if ($model->load(['Calendar' => Yii::$app->request->post()]) && $model->save()) {
            return ArrayHelper::toArray($model, [
                'common\models\Calendar' => [
                    'id',
                    'performer_id',
                    'start_date',
                    'end_date',
                    'description'
                ]
            ]);
        }
        return ['errors' => $model->errors()];
        }
        throw new ForbiddenHttpException;
    }

    /**
     * http://<HOST>/api/web/v1/calendar/delete
     * @method:POST
     * @return bool|false|int
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if (Yii::$app->user->can('delete', ['post' => $model->created_by])) {
            return $model->delete();
        }
        throw new ForbiddenHttpException;    }

    /**
     * Finds the Calendar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Calendar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Calendar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
