<?php

namespace api\modules\v1\controllers;

use common\models\Files;
use common\models\Foto;
use common\models\Genre;
use common\models\MainProfileInfo;
use common\models\UploadForm;
use Yii;
//use common\models\Attachment;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\web\Controller;

use yii\imagine\Image;

//use common\components\File;
//use Imagine\Imagick\Imagine;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use common\components\Uploader;
use yii\web\UploadedFile;

/**
 * Default controller for the `v1` module
 */
class DefaultController extends Controller
{

    /**
     * Renders the index view for the module
     * @return string
     *
     */
    public function actionIndex()
    {
//        return Attachment::upload();
    }

    public function actionCreate()
    {
//        return Attachment::upload();
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionBase()
    {
//        return Attachment::uploadBase64();
    }

    public function actionThumb()
    {
        $path = Yii::getAlias('@' . File::PATH_MODULE . 'user/image/10573dbc6c_20160714122527.jpg');
        $pathTo = Yii::getAlias('@' . File::PATH_MODULE . 'user/10573dbc6c_20160714122527.jpg');


        $imagine = new Imagine();
        $imagine->open($path)->thumbnail(new Box(100, 100), ImageInterface::THUMBNAIL_INSET)->save($pathTo);

        return true;
    }

    public function actionLoad()
    {
        $model = new Foto();

        if ($model->load(['UploadForm' => Yii::$app->request->post()])) {

            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file && $model->validate()) {

                FileHelper::createDirectory(
                    Yii::getAlias('@static-files/img/' . $model->file->extension), 0777);

                FileHelper::createDirectory(
                    Yii::getAlias('@static-files/img/thumbnail/' . $model->file->extension), 0777);

                $file_name = date('YmdHis') . '_' . $model->file->baseName . '.' . $model->file->extension;

                if ($model->file->saveAs(Yii::getAlias('@static-files/img/' . $model->file->extension) . '/' . $file_name)) {

                    $imagine = new Imagine();
                    $imagine->open(Yii::getAlias('@static-files/img/' . $model->file->extension) . '/' . $file_name)
                        ->thumbnail(new Box(100, 100), ImageInterface::THUMBNAIL_INSET)
                        ->save(Yii::getAlias('@static-files/img/thumbnail/' . $model->file->extension) . '/' . $file_name);
                }

                $foto = new Foto();
                $foto->path = $model->file->extension . '/' . $file_name;
                if ($foto->save(false)) {

                    return "['id' => $foto->id]";
                } else {
                    return ['errors' => $foto->errors()];
                }


            }
        }


        return false;
    }

    public function actionImage()
    {
        if (Genre::myupload()) {
            
            return true;
        }
        return false;
    }

}
