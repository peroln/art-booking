<?php

namespace api\modules\v1\controllers;

use common\components\File;
use common\components\helpers\ExtendedActiveRecord;
use common\components\Uploader;
use common\models\Album;
use common\models\Attachment;
use common\models\Calendar;
use common\models\Contact;
use common\models\Date;
use common\models\Event;
use common\models\Genre;
use common\models\NameEvent;
use common\models\Place;
use common\models\Role;
use common\models\Performer;
use common\models\Profile;
use common\models\search\UserSearch;
use common\models\UploadForm;
use Yii;
use common\models\User;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\FileHelper;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\web\UploadedFile;


/**
 * UserController implements the CRUD actions for User model.
 * Only 'admin' has access
 * param 'access-token' of admin is required everywhere
 */
class UserController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
           'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'create',
                'update',
                'user-update',
                'delete',
                'test',
                'event-create',
                'calendar-create',
                'calendar-view',
                'calendar-update',
                'calendar-delete',
                'video',
                'profile',
                'album-delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'create',
                'update',
                'user-update',
                'delete',
                'test',
                'event-create',
                'calendar-create',
                'calendar-view',
                'calendar-update',
                'calendar-delete',
                'video',
                'profile',
                'album-delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'update',
                        'delete',
                        'test',
                        'event-create',
                        'calendar-create',
                        'calendar-view',
                        'calendar-update',
                        'calendar-delete',
                        'video',
                        'profile',
                        'album-delete',
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'actions' => [
                        'create',
                        'user-update',
                    ],
                    'allow' => true,
                    'roles' => ['admin']
                ]
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'create' => ['POST'],
                'update' => ['PUT', 'POST'],
                'user-update' => ['PUT', 'POST'],
                'delete' => ['DELETE'],
                'view' => ['GET'],
                'album-view' => ['GET'],
                'test' => ['POST'],
                'event-create' => ['POST'],
                'calendar-create' => ['POST'],
                'calendar-view' => ['POST'],
                'calendar-update' => ['POST'],
                'calendar-delete' => ['POST'],
                'index' => ['GET'],
                'video' => ['POST'],
                'profile' => ['POST'],
                'album-delete' => ['POST'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Show all users
     * Method GET
     * param 'pageSize' (default = 10), '$pageCurrent'
     *
     * @return array with users 'models', count of all users 'count', 'currentPage', 'pagesCount'
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();

        if ($searchModel->load(['UserSearch' => Yii::$app->request->get()])) {

            $dataProvider = $searchModel->search();

            return [
                'models' => ArrayHelper::toArray($models = $dataProvider->getModels(), [
                    'common\models\User' => [
                        'id',
                        'first_name',
                        'middle_name',
                        'last_name',
                        'email',
                        'birthday',
                        'gender',
                        'url' => function ($model) {
                            /** @var $model User */
                            return Attachment::getUrl($model->id, 'avatar', 1);
                        },
                        'role' => function ($model) {
                            /** @var $model User */
                            if (isset($model->role0->name)) {
                                return $model->role0->name;
                            }
                            return '';
                        },
                        'status',
                        'created_at' => function ($model) {
                            /** @var $model User */
                            return date('c', $model->created_at);
                        },
                        'updated_at' => function ($model) {
                            /** @var $model User */
                            return date('c', $model->updated_at);
                        },
                        'updated_by',
                    ]
                ]),

                'count' => $dataProvider->query->count(),
                'currentPage' => $dataProvider->pagination->page,
                'pagesCount' => $dataProvider->pagination->pageCount
            ];
        }
        return ['errors' => $searchModel->errors()];
    }

    /**
     * Show user
     * Method GET
     * param 'id'
     * @return array||bool
     */
    public function actionView($id)
    {
        return [ArrayHelper::toArray($this->findModel($id), [
            'common\models\User' => [
                'id',
                'first_name',
                'middle_name',
                'last_name',
                'email',
                'birthday',
                'gender',
                'url' => function ($model) {
                    return Attachment::getUrl($model->id, 'avatar', 1);
                },
                'role' => function ($model) {
                    return Role::find()->where($model->role_id)->select('name')->one();
                },
                'status',
                'created_at',
                'created_by',
                'updated_at',
                'updated_by',
            ]
        ])
        ];
    }

    /**
     * Creates a new User.
     *
     * Method POST
     * param 'first_name', 'last_name', 'email', 'password' - required
     * 'middle_name' , 'city_id', 'tarif_id', 'role', 'phone', 'fax', 'icq', 'skype', 'privacy_id'
     *
     * @return array
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(['User' => Yii::$app->request->post()])) {

            $model->setPassword(Yii::$app->request->post('password'));
            $model->generateAuthKey();
            $model->role_id = Yii::$app->request->post('role_id');
            $model->save();
            return ['id' => $model->id];
        }
        return ['errors' => $model->errors()];
    }

    public function actionUserUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if ($model->load(['User' => Yii::$app->request->post()]) && $model->save()) {
            return ArrayHelper::toArray($model, [
                'common\models\User' => [
                    'id',
                    'first_name',
                    'middle_name',
                    'last_name',
                    'email',
                    'birthday',
                    'gender',
                    'url' => function ($model) {
                        /* @var $model User */
                        return Attachment::getUrl($model->id, 'avatar', 1);
                    },
                    'role_id',
                    'status',
                ]
            ]);
        }
        return ['errors' => $model->errors()];
    }


    /**
     * Updates an existing User.
     *
     * Method PUT
     * param 'id' - required
     * 'first_name', 'middle_name' 'last_name', 'email', 'city_id', 'tarif_id', 'role',
     * 'phone', 'fax', 'icq', 'skype', 'privacy_id'
     *
     * @return array
     */
    public function actionUpdate()
    {

        $model = $this->findModel(Yii::$app->request->post('id'));


        if ($model->load(['User' => Yii::$app->request->post()]) && $model->validate()) {

            if (Yii::$app->request->post('image') || Yii::$app->request->post('image_m')) {

                $input_names = ['image', 'image_m'];
                Attachment::uploadBase64($model->id, $input_names);
            }
            if (Yii::$app->request->post('avatar') || Yii::$app->request->post('avatar_m')) {
                $input_names = ['avatar', 'avatar_m'];
                Attachment::uploadBase64($model->id, $input_names);
            }


            if (!($profile = Profile::find()->where(['user_id' => $model->id])->one())) {

                $profile = new Profile();
            }

            if ($profile->load(['Profile' => Yii::$app->request->post()])) {

                $profile->user_id = $model->id;
                if (isset($profile->description_short)) {
                    $profile->title = $profile->description_short;
                }


                if (!$profile->save()) {

                    return ['errors' => $profile->errors()];
                }


                if (Yii::$app->request->post('genre_id')) {

                    if (!$performer = Performer::find()->where(['id' => $profile->id])->one()) {
                        $performer = new Performer();
                        $performer->status = 1;
                    }
                    if ($performer->load(['Performer' => Yii::$app->request->post()])) {

                        $performer->id = $profile->id;

                        if (!$performer->save()) {

                            return ['errors' => $performer->errors()];
                        }
                    }
                }
                if (!$contact = Contact::find()->where(['profile_id' => $profile->id])->one()) {
                    $contact = new Contact;
                }
                if ($contact->load(['Contact' => Yii::$app->request->post()]) && $contact->validate()) {
                    $contact->profile_id = $profile->id;
                    if (!$contact->save()) {
                        return ['errors' => $contact->errors()];
                    }
                }
            }
//            Update or create Albums names

            if (isset($profile->id)) {
                if ($id = Yii::$app->request->post('albom_id')) {
                    $album = Album::find()->where(['id' => $id])->one();

                } else {
                    $album = new Album();
                    $album->album_type = Yii::$app->request->post('album_type');
                    $album->profile_id = $profile->id;
                }
                if ($album->load(['Album' => Yii::$app->request->post()]) && $album->validate()) {

                    if ($album->save()) {
                        return true;
                    }
                    return $album;
                }
            }

//            Update password

            if ($model->password) {
                $model->setPassword($model->password);
            }

            if ($model->save()) {
                $model->birthday = date('Y-m-d', $model->birthday);
                return true;
            }
        }
        return ['errors' => $model->getErrors()];
    }

    /**
     * Set 'status' = 0
     *
     * Method DELETE
     * param 'id'
     * @return bool
     */
    public function actionDelete()
    {
        return $this->findModel(Yii::$app->request->post('id'))->delete();
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if ($model = User::findOne($id)) {
            return $model;
        }
        throw new NotFoundHttpException('404 Model not found.');
    }


//    TODO Profile получение информации текущего профиля
    /*
     *      auth_key - auth_key текущего юзера
     *
     */

    public function actionProfile()
    {
        return Yii::$app->user->identity;
        if ($auth_key = Yii::$app->request->post('auth_key')) {
            if (!($user = User::auth($auth_key))) {
                return ['auth_key' => false];
            };

            $result = [
                'id' => $user->id,
                'role_id' => $user->role_id,
                'first_name' => $user->first_name,
                'middle_name' => $user->middle_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'birthday' => date('Y-m-d', $user->birthday),
                'gender' => $user->gender,
                'avatar_m' => User::getAvatar($user->id),
                'image_m' => User::getImage($user->id),
            ];

            if ($profile = Profile::find()->where(['user_id' => $user->id])->one()) {

                $result['city_id'] = $profile->city_id;
                $result['description'] = $profile->description;
                $result['company_name'] = $profile->company_name;
                $result['description_short'] = $profile->title;
                $result['profile_id'] = $profile->id;


                if (($user->role_id === 1) && ($performer = Performer::find()->where(['id' => $profile->id])->one())) {

                    $result['genre_id'] = $performer->genre_id;
                    $result['performer_id'] = $performer->id;
                    if ($result['genre_id']) {
                        $genre = Genre::find()->select('name')->where(['id' => $result['genre_id']])->one();
                        $result['genre_name'] = $genre->name;
                    }
                }

                if ($contact = Contact::find()->where(['profile_id' => $profile->id])->one()) {
                    $result['phone'] = $contact->phone;
                    $result['site'] = $contact->site;
                    $result['facebook'] = $contact->facebook;
                    $result['twitter'] = $contact->twitter;
                    $result['vk'] = $contact->vk;
                    $result['google_plus'] = $contact->google_plus;
                    $result['lastfm'] = $contact->lastfm;
                    $result['youtube'] = $contact->youtube;
                }


            }
            if ($attachment = Attachment::find()->where(['obj_id' => $user->id, 'obj_type' => 'user'])->one()) {
                $result['image'] = Yii::getAlias('@root') . '/perol/' . $attachment->path;

            }
            if ($attachment = Attachment::find()->where(['obj_id' => $user->id, 'obj_type' => 'avatar'])->one()) {
                $result['avatar'] = Yii::getAlias('@root') . '/perol/' . $attachment->path;

            }
            return $result;

        }
        return 'Нет aut_key - параметра!';
    }



//TODO Получение альбомов , относящихся к определенному типу (foto, video, audio)
    /*
     * Получение альбомов , относящихся к определенному типу (foto, video, audio)
     * запрос host/.../user/album
     * profile_id - id профиля текущего юзера
     * auth_key - auth_key текущего юзера
     * album_type (1-foto, 2-video, 3-audio)
     * [['profile_id', 'album_type', 'auth_key'], require],
     * [['profile_id', 'album_type'], integer],
     * ['album_type', 'in', 'range'=>[1, 2, 3]]
     */

    public function actionAlbum()

    {
        if (($profile_id = Yii::$app->request->post('profile_id')) && ($auth_key = Yii::$app->request->post('auth_key'))
            && ($album_type = Yii::$app->request->post('album_type'))
        ) {

            switch ($album_type) {
                case 1 :
                    $type = 'fotos';
                    $type_id = 'foto_id';
                    break;
                case 2 :
                    $type = 'videos';
                    $type_id = 'video_id';
                    break;
                case 3 :
                    $type = 'audio';
                    $type_id = 'audio_id';
                    break;
                default:
                    return 'Нет такого типа альбома!';
            }
            $albums = (new \yii\db\Query())->select(['album.id as id', 'attachment.id as ' . $type_id, 'preview',
                'album.name', 'album.description', 'attachment.path as thumbnail'])
                ->from('album')->leftJoin('attachment', 'attachment.album_id = album.id ')
                ->where(['album.profile_id' => $profile_id, 'album.album_type' => $album_type])->all();
            if ($albums) {


                foreach ($albums as $key => $value) {

                    $a = [
                        'preview' => $albums[$key]['preview'] ? Yii::getAlias('@root') . '/perol/' . $albums[$key]['preview'] : Yii::getAlias('@root') . '/perol/static/web/v1/attachment/video/dfvfdv/video_default.png',
                        'name' => $albums[$key]['name'],
                        'description' => $albums[$key]['description'],
                        'album_id' => $albums[$key]['id'],
                        'attachment' => function ($albums, $key, $album_type, $type_id) {
                            if ($album_type === 2) {
                                return array('path' => $albums[$key]['thumbnail'], $type_id => $albums[$key][$type_id]);
                            }
                            if ($album_type === 3) {
                                return array('path' => Yii::getAlias('@root') . '/perol/' . $albums[$key]['thumbnail'], $type_id => $albums[$key][$type_id], 'audio_name' => substr(strrchr($albums[$key]['thumbnail'], '/'), 1));
                            }
                            return array('path' => Yii::getAlias('@root') . '/perol/' . $albums[$key]['thumbnail'], $type_id => $albums[$key][$type_id]);
                        }
                    ];

                    $a['attachment'] = $a['attachment']($albums, $key, $album_type, $type_id);
                    $result['albums'][] = $a;
                }
                $count = count($result['albums']) - 1;

                for ($i = 0; $i <= $count; $i++) {
                    if (isset($result['albums'][$i])) {
                        $result['albums'][$i][$type][] = $result['albums'][$i]['attachment'];
                        for ($s = $i + 1; $s <= $count; $s++) {
                            if (isset($result['albums'][$s])) {
                                if ($result['albums'][$i]['album_id'] === $result['albums'][$s]['album_id']) {
                                    $result['albums'][$i][$type][] = $result['albums'][$s]['attachment'];
                                    unset($result['albums'][$s]);

                                }
                            }
                        }
                    }
                }
                foreach ($result['albums'] as $key => $value) {
                    unset($result['albums'][$key]['attachment']);
                    $res['albums'][] = $result['albums'][$key];
                }
                return $res;
            }
            return 'Sorry, но альбомчиков-то пока нет!';
        }
        return 'Sorry, но доступ запрещен!';
    }

// TODO создание и обновление video альбома


    public function actionVideo()
    {

        if (($profile_id = Yii::$app->request->post('profile_id'))
            && ($album_type = Yii::$app->request->post('album_type'))
            && ($auth_key = Yii::$app->request->post('auth_key'))
        ) {

            if (!($album_id = Yii::$app->request->post('album_id'))) {
                $album = new Album();
                $attachment = new Attachment();
            } elseif ($album = Album::find()->where(['id' => $album_id])->one()) {

                $old_preview = $album->preview;
                $attachment = Attachment::find()->where(['album_id' => $album->id])->all();
            } else {
                return 'Вадим, это опять БАГ!!! Этот параметр albom_id = ' . $album_id . ' имеет проблемы!';
            }
            if ($album->load(['Album' => Yii::$app->request->post()])) {

                if (!$album->validate()) {
                    return ['errors' => $album->errors()];
                }
                $model = new UploadForm();

                if ((!empty($_FILES['preview'])) && is_array($_FILES['preview'])) {

                    $model->imageFile = UploadedFile::getInstanceByName('preview');
//                    return $model;

                    if ($model->validate()) {

                        if (!isset($old_preview)) {
                            $path = File::PATH_MODULE . 'video/' . rand();
                            FileHelper::createDirectory(Yii::getAlias('@perol') . '/' . $path, 0777);
                        } else {
                            $path = str_replace(strrchr($old_preview, '/'), '', $old_preview);
                        }

                        $model->imageFile->saveAs(Yii::getAlias('@perol') . '/' . $path . '/'
                            . $model->imageFile->name);
                        $album->preview = $path . '/' . $model->imageFile->name;
                        if ($album->preview && isset($old_preview)) {
                            Uploader::deleteFiles($old_preview);
                        }

                    } else {
                        return 'Uploaded files did not validate!';
                    }
                }
                if (empty($_FILES['preview']) || !(is_array($_FILES['preview']))) {
                    $album->preview = isset($old_preview) ? $old_preview : NULL;
                }
                if (!$album->save()) {
                    return ['errors' => $album->errors()];
                }


                for ($i = 0; $i < 50; $i++) {
                    if (!Yii::$app->request->post('path' . $i)) {
                        break;
                    }
                    $attachment = new Attachment();
                    $attachment->obj_type = 'profile';
                    $attachment->obj_id = $profile_id;
                    $attachment->album_id = $album->id;
                    $attachment->path = Yii::$app->request->post('path' . $i);


                    if (!$attachment->save()) {
                        return ['errors' => $attachment->errors()];
                    }
                }

                $attachment = (new Query())->select(['id AS video_id', 'path',])->from('attachment')->where(['album_id' => $album->id])->all();


                $res = ArrayHelper::toArray($album, [
                    'common\models\Album' => [
                        'id',
                        'preview' => function ($album) {
                            return $album->preview ? Yii::getAlias('@root') . '/perol/' . $album->preview : Yii::getAlias('@root') . '/perol/static/web/v1/attachment/video/dfvfdv/video_default.png';
                        },
                        'album_id' => 'id',
                        'name',
                        'description'
                    ]
                ]);


                return ['albums' => $res, 'attachment' => $attachment,];

            } elseif (isset($old_preview)) {
                $album->preview = $old_preview;
            }
            //                    create attachment
            if (isset($album->id)) {
                for ($i = 0; $i < 20; $i++) {
                    if (!Yii::$app->request->post('path' . $i)) {
                        break;
                    }
                    $attachment = new Attachment();
                    $attachment->obj_type = 'profile';
                    $attachment->obj_id = $profile_id;
                    $attachment->album_id = $album->id;
                    $attachment->path = Yii::$app->request->post('path' . $i);
                    if ($file_name = Yii::$app->request->post('file_name')) {
                        $attachment->name = $file_name;
                    }

                    if (!$attachment->save()) {
                        return ['errors' => $attachment->errors()];
                    }
                }

                if (!$album->save()) {
                    return ['errors' => $album->errors()];
                }
            }


            $res = ArrayHelper::toArray($album, [
                'common\models\Album' => [
                    'id',
                    'preview' => function ($album) {
                        return $album->preview ? Yii::getAlias('@root') . '/perol/' . $album->preview : Yii::getAlias('@root') . '/perol/' . '/perol/static/web/v1/attachment/video/dfvfdv/video_default.png';
                    },
                    'album_id' => 'id',
                    'name',
                    'description'
                ]
            ]);
            $attachment = (new Query())->select(['id AS video_id', 'path',])->from('attachment')->where(['album_id' => $album_id])->all();
            return ['albums' => $res, 'attachment' => $attachment,];
        }
        return 'Не хватает параметров! Ты опять что-то забыл прислать! Проверь еще раз прежде чем меня беспокоить!';
    }


//    album delete

    public function actionAlbumDelete()
    {
        if (($auth_key = Yii::$app->request->post('auth_key')) && ($album_id = Yii::$app->request->post('album_id'))) {
            $result = (new Query())->select('album.id AS album_id')->from('album')->leftJoin('profile', 'profile.id = album.profile_id')->leftJoin('user', 'user.id = profile.user_id')->where(['user.auth_key' => $auth_key, 'album.id' => $album_id])->one();

            if ($result) {
                (new Query())->createCommand()->delete('attachment', ['album_id' => $result['album_id']])->execute();
                (new Query())->createCommand()->delete('album', ['id' => $result['album_id']])->execute();
                return true;
            }
            return false;
//            return $this->redirect(['album', 'album_type' => 2, 'profile_id' => 20, 'auth_key' => 'CS1VlusoRb0TqOljd1gL9Sarytni---E']);
        }

    }


//    attacment delete
    /*
     * удаление отдельного attachment (файл видео, аудио или фото)
     * host/.../user/attachment-delete
     * 'auth_key' - auth_key текущего юзера
     * 'attachment_id' - id удаляемого attachment(файла)
     * [['attachment_id','auth_key'], request]
     * answer - true or false
     */

    public function actionAttachmentDelete()
    {
        if (($auth_key = Yii::$app->request->post('auth_key'))
            && ($attachment_id = Yii::$app->request->post('attachment_id'))
        ) {
            $result = (new Query())->select(['attachment.id AS attachment_id', 'attachment.path AS path'])->from('attachment')
                ->leftJoin('album', 'album.id = attachment.album_id')
                ->leftJoin('profile', 'profile.id = album.profile_id')
                ->leftJoin('user', 'user.id = profile.user_id')
                ->where(['user.auth_key' => $auth_key, 'attachment.id' => $attachment_id])->one();
            if ($result) {
                $path = $result['path'];
                if ((strpos($path, 'http://') === false) && (strpos($path, File::PATH_MODULE) !== false)) {
                    Uploader::deleteFiles($path);
                }

                (new Query())->createCommand()->delete('attachment', ['attachment.id' => $result['attachment_id']])->execute();

                return true;
            }
            return false;

        }
    }


// TODO create audio and foto albums
    /*
     * host/.../user/album-create
     * [['name', 'profile_id', 'album_type', 'auth_key'], 'required'],
     * [['description', 'preview'], 'string'],
     * 'files' - файлы
     */
    public function actionAlbumCreate()
    {
        if (($album_type = Yii::$app->request->post('album_type'))
            && ($profile_id = Yii::$app->request->post('profile_id'))
            && ($auth_key = Yii::$app->request->post('auth_key'))
        ) {
            $album = new Album();
            switch ($album_type) {
                case 1 :
                    $type = 'fotos';
                    $type_id = 'foto_id';
                    break;
                case 2 :
                    $type = 'videos';
                    $type_id = 'video_id';
                    break;
                case 3 :
                    $type = 'audio';
                    $type_id = 'audio_id';
                    break;
                default:
                    return 'Нет такого типа альбома!';
            }

            if ($album->load(['Album' => Yii::$app->request->post()]) && $album->validate()) {
                $album->album_type = $album_type;
                if (!$album->save()) {
                    return ['errors' => $album->errors()];
                }
                if (!(empty($_FILES['preview'])) && is_array($_FILES['preview'])) {

                    $model = new UploadForm();
                    $model->scenario = $type;
                    $model->imageFile = UploadedFile::getInstanceByName('preview');
                    $path = Yii::getAlias('@' . File::PATH_MODULE) . $type . '/' . $album->id;
                    FileHelper::createDirectory($path, 0777);
                    $model->preview = $path;
                    if ($file_name = $model->upload()) {

                        $album->preview = File::PATH_MODULE . $type . '/' . $album->id . $file_name;

                    } else {
                        return ['errors' => $model->errors()];
                    }

                } else {
                    $album->preview = NULL;
                }
                if (!$album->save()) {
                    return ['errors' => $album->errors()];


                } // create and describe attachment

                elseif (!empty($_FILES['files']) && is_array($_FILES['files'])) {

                    $model = new UploadForm();
//                    $model_f->scenario = 'audio';
                    $model->imageFiles = UploadedFile::getInstancesByName('files');
                    $path = Yii::getAlias('@' . File::PATH_MODULE) . $type . '/' . $album->id;
                    $model->preview = $path;

                    if (!$model->validate()) {
                        return [
                            's' => $model->scenario,
                            'e' => $model->getErrors(),
                            'image' => $model->imageFiles
                        ];
                    };
                    FileHelper::createDirectory($path, 0777);
                    foreach ($model->imageFiles as $file) {
                        $file->saveAs($model->preview . '/' . $file->name);
                        $attach = new Attachment();
                        $attach->path = File::PATH_MODULE . $type . '/' . $album->id . '/' . $file->name;
                        $attach->album_id = $album->id;
                        $attach->obj_type = 'profile';
                        $attach->obj_id = $profile_id;
                        $attach->type = $album_type;
                        if (!$attach->save()) {
                            return ['errors' => $attach->errors()];
                        }
                    }
                }
                return $album->id;
            }
            return false;
        }
    }




// TODO Просмотр одного альбома
    /*
     * Просмотр одного альбома
     *  host/.../user/album-view
     *  method GET
     * album_id - id просматриваемого альбома
     * [['album_id'], request]
     * return:
     * [
     *  'album_type' - тип алббома (1-foto, 2-video, 3-audio)
     *  'attachment': - присоединяемый файл
     *      [{'path', - url присоединяемого файла
     *        'attachment_id' - id присоединяемого файла
     *      },…]
     *  'description' - описание альбома
     *  'id' - id альбома
     *  'name' - название альбома
     *  'preview' - адрес фото абложки альбома
     * ]
     */

    public function actionAlbumView()
    {
        if ($album_id = Yii::$app->request->get('album_id')) {
            $results = (new Query())->select(['album.id AS id', 'album.name AS name', 'album_type',
                'album.description AS description', 'preview', 'path', 'attachment.id AS attachment_id'])
                ->from('album')
                ->leftJoin('attachment', 'attachment.album_id = album.id')
                ->where(['album.id' => $album_id])->all();
            foreach ($results as $key => $result) {
                $res['attachment'][] = ['path' => $results[$key]['path'], 'attachment_id' => $results[$key]['attachment_id']];
                $res['album_type'] = $results[$key]['album_type'];
                $res['description'] = $results[$key]['description'];
                $res['id'] = $results[$key]['id'];
                $res['name'] = $results[$key]['name'];
                $res['preview'] = Yii::getAlias('@root') . '/perol/' . $results[$key]['preview'];
            }
            return $res;
        }
        return 'Ypu did not send "album_id"! Or this album is missing!';
    }


    public function actionTest()
    {
        if (!empty($_FILES['files'])) {
            return 'Ура, заработало!!!';
        }
        return 'Саня, увы, но файлы не пришли!';
    }


//TODO Обновление альбома
    /*
     * Обновление альбома
     * host/.../user/album-update
     * 'auth_key'- auth_key текущего юзера
     * 'album_id' - id просматриваемого альбома
     * ['album_id','auth_key'],request]
     * 'name' - название альбома
     * 'profile_id' - id профиля текущего юзера
     * 'album_type' (1-foto, 2-video, 3-audio)
     * 'description' - описание альбома
     * 'preview' - адрес фото обложки альбома
     *
     */

    public function actionAlbumUpdate()
    {

        if (($auth_key = Yii::$app->request->post('auth_key')) && ($album_id = Yii::$app->request->post('album_id'))) {

            $author = (new Query())->from('album')
                ->leftJoin('profile', 'profile.id = album.profile_id')
                ->leftJoin('user', 'user.id = profile.user_id')
                ->where(['album.id' => $album_id])->one();
            if (!($auth_key === $author['auth_key'])) {
                return 'Sorry, но Вы редактировать этот альбом не можете!';
            }
            if ($album = Album::find()->where(['id' => $album_id])->one()) {
                switch ($album->album_type) {
                    case 1 :
                        $type = 'fotos';
                        $type_id = 'foto_id';
                        break;
                    case 2 :
                        $type = 'videos';
                        $type_id = 'video_id';
                        break;
                    case 3 :
                        $type = 'audio';
                        $type_id = 'audio_id';
                        break;
                    default:
                        return 'Нет такого типа альбома!';
                }
                $old_preview = $album->preview;

                if ($album->load(['Album' => Yii::$app->request->post()])) {

                    if (!$album->validate()) {
                        return ['errors' => $album->errors()];
                    }
                    if (!empty($_FILES['preview']) && is_array($_FILES['preview'])) {

                        $model = new UploadForm();
                        $model->imageFile = UploadedFile::getInstanceByName('preview');
                        if ($model->imageFile) {
                            $path = Yii::getAlias('@' . File::PATH_MODULE) . $type . '/' . $album->id;
                            FileHelper::createDirectory($path, 0777);
                            $model->preview = $path;

                            if ($file_name = $model->upload()) {

                                $album->preview = File::PATH_MODULE . $type . '/' . $album->id . $file_name;
                                if (isset($old_preview)) {
                                    Uploader::deleteFiles($old_preview);
                                }

                            } else {
                                return ['errors' => $model->errors()];
                            }
                        } else {
                            return ['errors' => $model->errors()];
                        }


                    } elseif (isset($old_preview)) {
                        $album->preview = $old_preview;
                    } else {
                        $album->preview = NULL;
                    }
                    if (!$album->save()) {
                        return ['errors' => $album->errors()];
                    }

//                    update attachment
//                    ************************************


                    if (!empty($_FILES['files']) && is_array($_FILES['files'])) {
                        $models = new UploadForm();
                        $models->scenario = $type;

                        $models->imageFiles = UploadedFile::getInstancesByName('files');
                        $path = Yii::getAlias('@' . File::PATH_MODULE) . $type . '/' . $album->id;
                        $models->preview = $path;
//                        return $models->imageFiles;
                        if ($models->validate() && $models->imageFiles) {

                            FileHelper::createDirectory($path, 0777);
                            foreach ($models->imageFiles as $file) {
                                $file->saveAs($models->preview . '/' . $file->name);
                                $attach = new Attachment();
                                $attach->path = File::PATH_MODULE . $type . '/' . $album->id . '/' . $file->name;
                                $attach->album_id = $album->id;
                                $attach->obj_type = 'profile';
                                $attach->type = $album->album_type;
                                $attach->obj_id = $album->profile_id;
                                if (!$attach->save()) {
                                    return ['errors' => $attach->errors()];
                                }
                            }
                        } else {
                            return ['model' => $models->errors()];
                        }
                    }
                    $album = (new Query())->select(['id AS album_id', 'preview', 'name', 'description', 'profile_id', 'album_type'])
                        ->from('album')->where(['id' => $album->id])->one();
                    $album['preview'] = Yii::getAlias('@root') . '/perol/' . $album['preview'];
                    $attachments = (new Query())->select(['path', 'id AS ' . $type_id])->from('attachment')
                        ->where(['album_id' => $album['album_id']])->all();
                    foreach ($attachments as $key => $attachment) {
                        $attachments[$key]['path'] = Yii::getAlias('@root') . '/perol/' . $attachments[$key]['path'];
                    }
                    return [
                        'album' => $album,
                        'attachment' => $attachments,
                    ];


//                    send album and all attachment

//                    $results = (new Query())->select(['album.id AS id', 'album.name AS name', 'album_type',
//                        'album.description AS description', 'preview', 'path', 'attachment.id AS attachment_id'])
//                        ->from('album')
//                        ->leftJoin('attachment', 'attachment.album_id = album.id')
//                        ->where(['album.id' => $album_id])->all();
//                    foreach ($results as $key => $result) {
//                        $res['attachment'][] = ['path' => $results[$key]['path'], 'attachment_id' => $results[$key]['attachment_id']];
//                        $res['album_type'] = $results[$key]['album_type'];
//                        $res['description'] = $results[$key]['description'];
//                        $res['id'] = $results[$key]['id'];
//                        $res['name'] = $results[$key]['name'];
//                        $res['preview'] = Yii::getAlias('@root') . '/perol/' . $results[$key]['preview'];
//                    }
//                    return $res;

//                    ************************************
                }
                return 'Что-то не загрузилось!';
            }
            return 'Нет такого альбома!';
        }
        return 'Не хватает параметров';
    }


    // TODO CALENDAR Занесение календарных данных артиста, в которые он будет недоступен

    /*
     *  Method POST
     *  host/.../user/calendar-create
     *  'auth_key' - auth_key артиста
     *  'performer_id' - performer_id артиста
     *  'date'[] -  array дат события
     *  'description' - описание события
     *  [['performer_id', 'auth_key', 'date'], 'required']
     *  [['description'], 'string']
     *
     */
    public function actionCalendarCreate()
    {
        if (($authkey = Yii::$app->request->post('auth_key')) && ($performer_id = Yii::$app->request->post('performer_id'))
            && ($dates = Yii::$app->request->post('date')) && is_array($dates)) {
            $calendar = new Calendar();
            if ($calendar->load(['Calendar' => Yii::$app->request->post()])) {
                $nameEvent = new NameEvent();
                if ($nameEvent->load(['NameEvent' => Yii::$app->request->post()]) && $nameEvent->save()) {

                    foreach ($dates as $date) {
                        $newDate = new Date();
                        $newDate->name_event_id = $nameEvent->id;
                        $newDate->date = strtotime($date);
                        if (!$newDate->save()) {
                            return 'Дата не была сохранена!';
                        }


                        $calendar->name_event_id = $nameEvent->id;
                        $calendar->performer_id = $performer_id;
                    }
                    if (!$calendar->save()) {
                        return 'Событие календаря не удалось сохранить!';
                    }
                    return true;

                }
                return 'Не удалось сохранить событие в NameEvent!';
            }
            return false;
        }
        return 'Не хватает параметров!';
    }


//    TODO Create custamers EVENT
/*
 * Method POST
 * host/.../event-create
 * 'auth_key'
 * 'profile_id'
 * 'date' - array content dates of the current event
 * 'city_id'
 * 'latitude'
 * 'longitude'
 * 'description' - название самого события
 * 'name' название кафе, ресторана, клуба где будет происходить событие
 * 'address'
 * 'imageFile' - photo
 *  [['city_id', 'latitude', 'longitude', ], 'integer'],
            [['name', 'address',], 'required'],
            [['name'], 'string', 'max' => 150],
            [['address'], 'string', 'max' => 255],
            [['city_id'
 *
 * [['place_id', 'profile_id', 'album_id', 'name_event_id', 'status'], 'integer'],
            [['name', 'name_event_id', 'status'], 'required'],
            [['name']],
            [['description']],
            [['album_id'], ],
            [['profile_id'],],

 */

public function actionEventCreate(){

    if (($authkey = Yii::$app->request->post('auth_key')) && ($profile_id = Yii::$app->request->post('profile_id'))
        && ($dates = Yii::$app->request->post('date')) && is_array($dates)) {


        $event = new Event();
        if ($event->load(['Event' => Yii::$app->request->post()]) && $event->validate()) {
            $nameEvent = new NameEvent();
            if ($nameEvent->load(['NameEvent' => Yii::$app->request->post()]) && $nameEvent->save()) {
                $place = new Place();
if($place->load(['Place' => Yii::$app->request->post()]) && $place->save()) {


    foreach ($dates as $date) {
        $newDate = new Date();
        $newDate->name_event_id = $nameEvent->id;
        $newDate->date = strtotime($date);
        if (!$newDate->save()) {
            return 'Дата не была сохранена!';
        }


        $event->name_event_id = $nameEvent->id;
        $event->place_id = $place->id;
    }
    if (!$event->save()) {
        return 'Событие календаря не удалось сохранить!';
    }
    $image = new UploadForm();
//    if(($imageFile = UploadedFile::getInstanceByName('imageFile')) && $image->validate()){
    if(($image->imageFile = UploadedFile::getInstanceByName('imageFile')) && $image->validate()){
        $path = File::PATH_MODULE . 'event/' . $event->id;
//return var_dump($imageFile);exit;
        FileHelper::createDirectory(Yii::getAlias('@perol') . '/' . $path, 0777);
        if($image->imageFile->saveAs(Yii::getAlias('@perol') . '/' . $path . '/'. $image->imageFile->name)){
            $attachment = new Attachment();
            $attachment->obj_type = 'event';
            $attachment->obj_id = $event->id;

            $attachment->name = $image->imageFile->name;
            $attachment->path = $path .'/'. $image->imageFile->name;
            if(!$attachment->save()){
                return 'Attachment not saved';
            }
        }

    }
    return true;
}
return 'Не удалось сохранить место в таблице place';
            }
            return 'Не удалось сохранить событие в NameEvent!';
        }
        return false;
    }
}
// TODO Просмотр событий календаря определенного артиста
    /*
     *  Method POST
     * host/.../user/calendar-view
     * 'auth_key' - auth_key артиста
     * 'performer_id' - performer_id артиста
     *  [['performer_id', 'auth_key',], require]
     *
     */
    public function actionCalendarView()
    {
        if (($authkey = Yii::$app->request->post('auth_key')) && ($performer_id = Yii::$app->request->post('performer_id'))) {
            $calendar = (new Query())->select(['name_event.description', 'calendar.id AS calendar_id', 'calendar.name_event_id',
                'calendar.performer_id', 'date.date', 'date.id AS date_id'])
                ->from('calendar')
                ->leftJoin('name_event', 'name_event.id = calendar.name_event_id')
                ->leftJoin('date', 'date.name_event_id = name_event.id')->where(['performer_id' => $performer_id])->all();
            foreach ($calendar as $key => $value) {
                $calendar[$key]['date'] = date('Y-m-d', $calendar[$key]['date']);
//                $calendar[$key]['end_date'] = date('Y-m-d', $calendar[$key]['end_date']);
            }
            return $calendar;
        }
        return 'Не хватает параметров!';
    }
// TODO Изменение события календаря определенного артиста
    /*
     *  Method POST
     * host/.../user/calendar-update
     * 'auth_key' - auth_key артиста
     * 'performer_id' - performer_id артиста
     *  'name_event_id' - id редактируемого события
     * [['name_event_id', 'performer_id', 'auth_key',], require]
     *  'date'[] - array дат события
     *  'description' - описание события
     *  [['description', 'data'[]] 'string']
     *
     */
    public function actionCalendarUpdate()
    {
        if (($authkey = Yii::$app->request->post('auth_key'))
            && ($performer_id = Yii::$app->request->post('performer_id'))
            && ($name_event_id = Yii::$app->request->post('name_event_id'))) {
            if (!(new Query())->from('calendar')->where(['performer_id' => $performer_id])->one()) {
                return 'Sorry, но Вы не можете редактировать это событие!';
            }
            $name_event = NameEvent::find()->where(['id' => $name_event_id])->one();
            if ($dates = Yii::$app->request->post('date')) {
                if (is_array($dates)) {
                    foreach ($dates as $date) {
                        $newDate = new Date();
                        $newDate->name_event_id = $name_event_id;
                        $newDate->date = strtotime($date);
                        if (!$newDate->save()) {
                            return 'Дата не была сохранена!';
                        }
                    }
                } else {
                    return 'Date must be array!';
                }
            }
            if (!($name_event->load(['NameEvent' => Yii::$app->request->post()]) && $name_event->save())) {
                return false;
            }
            return true;
        }
        return 'Не хватает параметров!';
    }

    // TODO Удаление события календаря определенного артиста
    /*
     *  Method POST
     * host/.../user/calendar-delete
     * 'auth_key' - auth_key артиста
     * 'performer_id' - performer_id артиста
     *  'calendar_id' - id удаляемого события
     * 'date' - array удаляемых дат
     * присылать надо либо id календарного события(при этом будут удалены и все даты этого события),
     * либо date_id - id удаляемой даты
     * [[['calendar_id' || 'date_id'], 'performer_id', 'auth_key',], require]
     *
     */
    public function actionCalendarDelete()
    {
        if (($authkey = Yii::$app->request->post('auth_key')) && ($performer_id = Yii::$app->request->post('performer_id'))
            && ((($calendar_id = Yii::$app->request->post('calendar_id')) || ($date_id = Yii::$app->request->post('date_id'))))) {
            if (!(new Query())->from('calendar')->where(['performer_id' => $performer_id])->one()) {
                return 'Sorry, но Вы не можете удалить это событие!';
            }
//            return $calendar = Calendar::find()->where(['id' =>$calendar_id])->one();
            if (isset($calendar_id) && ($calendar = Calendar::find()->where(['id' =>$calendar_id])->one())) {
//                return $calendar;

                    if(Date::deleteAll(['name_event_id' => $calendar->name_event_id]) &&
                    Calendar::deleteAll(['id' => $calendar_id])){
                        NameEvent::deleteAll(['id' => $calendar->name_event_id]);
                        return true;
                    }




            }
                if(isset($date_id) && Date::deleteAll(['id' => $date_id])){
                    return true;
                }


            return false;
        }
        return 'Не хватает параметров!';
    }
}
