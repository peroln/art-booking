<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Attachment;
use common\models\search\AttachmentSearch;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AttachmentController implements the CRUD actions for Attachment model.
 */
class AttachmentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

//        $behaviors['authenticator'] = [
//            'class' => QueryParamAuth::className(),
//            'only' => [
//                'create',
//                'update',
//                'delete',
//                'view',
//                'index'
//            ],
//        ];

//        $behaviors['access'] = [
//            'class' => AccessControl::className(),
//            'only' => [
//                'create',
//                'update',
//                'delete',
//                'view',
//                'index'
//            ],
//            'rules' => [
//                [
//                    'actions' => [
//                        'create',
//                        'update',
//                        'delete',
//                        'view',
//                        'index'
//                    ],
//                    'allow' => true,
//                    'roles' => ['admin'],
//                ],
//            ],
//        ];
//
        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'create' => ['POST'],
                'update' => ['PUT'],
                'delete' => ['DELETE'],
                'view' => ['GET'],
                'index' => ['GET'],
                'role' => ['POST'],
                'upload' => ['post']
            ],
        ];
        return $behaviors;
    }

    /**
     * http://<HOST>/api/web/v1/attachment/index
     * @method: GET
     * @params: page - номер страницы, size - количество выводимых записей.
     * @return array
     */
    public function actionIndex()
    {
        $searchModel = new AttachmentSearch();

        if ($searchModel->load(['AttachmentSearch' => Yii::$app->request->get()]) && $searchModel->validate()) {

            $dataProvider = $searchModel->search();

            return [
                'models' => ArrayHelper::toArray($dataProvider->getModels(), [
                    'common\models\Attachment' => [
                        'id',
                        'genre_id',
                        'obj_id',
                        'obj_type',
                        'name',
                        'type'=> function($model){
                            /** @var $model Attachment */
                            if(isset($model->type0->name)){
                                return $model->type0->name;
                            }
                            return '';
                        },
                        'path',
                        'thumbnail',
                        'show',
                        'status'
                    ]
                ]),
                'count' => $dataProvider->query->count(),
                'currentPage' => $dataProvider->pagination->page,
                'pagesCount' => $dataProvider->pagination->pageCount
            ];
        }
        return ['errors' => $searchModel->errors()];
    }

    /**
     * http://<HOST>/api/web/v1/attachment/view
     * @method: GET
     * @param integer $id
     * @return array []
     */
    public function actionView($id)
    {
        return ArrayHelper::toArray($this->findModel($id), [
            'common\models\Attachment' => [
                'id',
                'genre_id',
                'obj_id',
                'obj_type',
                'name',
                'type',
                'path',
                'thumbnail',
                'show',
                'status'
            ]
        ]);
    }

    /**
     * Creates a new Attachment model.
     *
     * http://<HOST>/api/web/v1/attachment/create
     * @method: POST
     * @return array
     */
    public function actionCreate()
    {
        $model = new Attachment();

        if ($model->load(['Attachment' => Yii::$app->request->post()]) && $model->save()) {

            return ['id' => $model->id];
        }
        return ['errors' => $model->errors()];
    }

    /**
     * Updates an existing Attachment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * http://<HOST>/api/web/v1/attachment/update
     * @method: POST
     * @return array
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if ($model->load(['Attachment' => Yii::$app->request->post()]) && $model->save()) {
            return ArrayHelper::toArray($model, [
                'common\models\Attachment' => [
                    'id',
                    'genre_id',
                    'obj_id',
                    'obj_type',
                    'name',
                    'type',
                    'path',
                    'thumbnail',
                    'show',
                    'status'
                ]
            ]);
        }
        return ['errors' => $model->errors()];
    }

    /**
     * http://<HOST>/api/web/v1/attachment/delete
     * @method:POST
     * @return bool|false|int
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        return $this->findModel(Yii::$app->request->post('id'))->delete();
    }

    /**
     * Finds the Attachment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attachment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Attachment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
