<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\City;
use common\models\search\CitySearch;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [
                'create',
                'update',
                'delete',
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'create' => ['POST'],
                'update' => ['PUT', 'POST'],
                'delete' => ['POST', 'DELETE'],
                'view' => ['GET'],
                'index' => ['GET'],
            ],
        ];
        return $behaviors;
    }

    /**
     * http://<HOST>/api/web/v1/city/index
     * @method: GET
     * @params: page - номер страницы, size - количество выводимых записей.
     * @return array
     */
    public function actionIndex()
    {
        $searchModel = new CitySearch();
        if ($searchModel->load(['CitySearch' => Yii::$app->request->get()]) && $searchModel->validate()) {

            $dataProvider = $searchModel->search();
            $models = $dataProvider->getModels();

            return [
                'models' => ArrayHelper::toArray($models, [
                    'common\models\City' => [
                        'id',
                        'region' => function($model){
                            /** @var $model City */
                            if($model->region->name){
                                return $model->region->name;
                            }
                            return '';
                        },
                        'name',
                        'status'
                    ]
                ]),
                'count' => $dataProvider->query->count(),
                'currentPage' => $dataProvider->pagination->page,
                'pagesCount' => $dataProvider->pagination->pageCount
            ];
        }
        return ['errors' => $searchModel->errors()];
    }

    /**
     * http://<HOST>/api/web/v1/city/view
     * @method: GET
     * @param integer $id
     * @return array []
     */
    public function actionView($id)
    {
       return ArrayHelper::toArray($this->findModel($id), [
           'common\models\City' => [
               'id',
               'region' => function($model){
                   /** @var $model City */
                   if($model->region->name){
                       return $model->region->name;
                   }
                   return '';
               },
               'name',
               'status'
           ]
       ]);
    }

    /**
     * Creates a new City model.
     *
     * http://<HOST>/api/web/v1/city/create
     * @method: POST
     * @return array
     */
    public function actionCreate()
    {
        $model = new City();

        if ($model->load(['City' => Yii::$app->request->post()]) && $model->save()) {
            return ['id' => $model->id];
        }
        return ['errors' => $model->errors()];
    }

    /**
     * Updates an existing City model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * http://<HOST>/api/web/v1/city/update
     * @method: POST
     * @return array
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if ($model->load(['City' => Yii::$app->request->post()]) && $model->save()) {
            return ArrayHelper::toArray($model, [
                'common\models\City' => [
                    'id',
                    'region' => function($model){
                        /** @var $model City */
                        if($model->region->name){
                            return $model->region->name;
                        }
                        return '';
                    },
                    'name',
                    'status'
                ]
            ]);
        }
        return ['errors' => $model->errors()];
    }

    /**
     * http://<HOST>/api/web/v1/city/delete
     * @method:POST
     * @return bool|false|int
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        return $this->findModel(Yii::$app->request->post('id'))->delete();
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return City the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = City::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
