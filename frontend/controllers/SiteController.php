<?php
namespace frontend\controllers;

use common\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\site\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use common\models\site\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout', 'signup'],
//                'rules' => [
//                    [
//                        'actions' => ['signup'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
//    public function actionSocseti(){
//
//        return $this->render('socseti');
//
//    }


    public function actionIndex()
    {
//            if ($req = Yii::$app->request->get('code')) {
//
//                $ch = curl_init('https://oauth.vk.com/access_token?client_id=5576983&client_secret=yfLEXzoIeoJfPxSArN5M&code=' . $req . '&redirect_uri=http://firstteam.vt-host.co.ua/perol/frontend/site');
//                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                $new_user = curl_exec($ch); // выполняем запрос curl
//                curl_close($ch);


//                $new_user = substr($new_user, 1, -1);
//                $new_user = $this->strToArray($new_user);
//
////                var_dump($new_user); exit;
//                $new_user['email'] = !empty($new_user['email']) ? $new_user['email'] : null;
//
//                if ($new_user['user_id'] != NULL) {
//                    $chn = curl_init('https://api.vk.com/method/users.get?fields=photo_id,verified,sex,bdate&access_token='. $new_user['access_token'] .'&v=5.53');
//                    curl_setopt($chn, CURLOPT_RETURNTRANSFER, true);
//                    $new_info = curl_exec($chn); // выполняем запрос curl
//                    curl_close($chn);
//                    $new_info = str_replace('"response":', '', $new_info);
//                    $new_info = substr($new_info, 3, -3);
//                    $new_info = $this->strToArray($new_info);
//                    $new_info['email'] = $new_user['email'];
//                      if($new_info['email'] == NULL){
//                          $model = new SignupForm();
//                          $model->first_name = $new_info['first_name'];
//                          $model->last_name = $new_info['last_name'];
//                          $model->gender = $new_info['sex'] == '2' ?  'male' : 'female';
//                          $model->password = '12345678';
//
//                          return $this->render('getemail', ['model' => $model]);
//                }
//                    if($user = User::find()->where(['email' => $new_info['email']])->one()){
////var_dump($user); exit;
//                        return $this->render('socseti', ['user' => $user->getAuthKey()]);
//                    }
//                    $user = new User();
//                    $user->first_name = $new_info['first_name'];
//                    $user->last_name = $new_info['last_name'];
//                    $user->email = $new_info['email'];
//                    $user->password = $user->setPassword('12345678');
//                    $user->generateAuthKey();
//                    $user->gender = $new_info['sex'] == '2' ?  'male' : 'female';
////                    var_dump($user); exit;
//                    if($user->save()){
//                        return $this->render('socseti', ['user' =>  $user->id]);
//                    }
//
//                    return $this->render('socseti', ['user' =>  $new_info]);
//                }
//
//            }
//return $this->render('index');
        if ($req = Yii::$app->request->get('code')) {

            $ch = curl_init('https://graph.facebook.com/oauth/access_token?client_id=295284927499933&redirect_uri=http://firstteam.vt-host.co.ua/perol/frontend/site&client_secret=6dd017449008b739343d0a11970bb54f&code=' . $req);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $new_info = curl_exec($ch); // выполняем запрос curl
            curl_close($ch);

            $new_info = explode("&", $new_info);

            foreach ($new_info as $n_user) {
                $n_u = explode("=", $n_user);
                $new_inf[$n_u[0]] = $n_u[1];
            }


            if ($new_inf['access_token']) {
                $chr = curl_init('https://graph.facebook.com/me?access_token=' . $new_inf['access_token'] . '&fields=first_name,last_name,email,birthday,gender&auth_type=rerequest&scope=email');
                curl_setopt($chr, CURLOPT_RETURNTRANSFER, true);
                $new_user = curl_exec($chr); // выполняем запрос curl
                curl_close($chr);


                $new_user = json_decode($new_user);

                $new_user->email = property_exists($new_user, 'email') ? $new_user->email : $new_user->id. '@gmail.com';

                if ($new_user->email == NULL) {
                    $model = new SignupForm();
                    $model->first_name = $new_user->first_name;
                    $model->last_name = $new_user->last_name;
                    $model->gender = $new_user->gender == 'male' ? 'male' : 'female';
                    $model->password = '12345678';
                    $model->email =  $new_user->id. '@gmail.com';
//
                    return var_dump($model);
                }
                if ($user = User::find()->where(['email' => $new_user->email])->one()) {

                    return $user->getAuthKey();
                }

                $user = new User();
                $user->first_name = $new_user->first_name;
                $user->last_name = $new_user->last_name;
                $user->email = $new_user->email;
                $user->password = $user->setPassword('12345678');
                $user->generateAuthKey();
                $user->gender = $new_user->gender;
                if ($user->save(false)) {

                    return $user->id;
                }

                return $this->errors();


            }
            return $this->errors();
        }
        return $this->render('index');
    }


    public function strToArray($string)
    {

        $string = str_replace('"', '', $string);
        $array = explode(",", $string);
        foreach ($array as $n_user) {
            $n_u = explode(":", $n_user);
            $result [$n_u[0]] = $n_u[1];
        }
        return $result;
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {

            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->render('user', [
                        'id' => $user->id,
                    ]);
                }

            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionUser()
    {
        return $this->render('user');
    }

}
