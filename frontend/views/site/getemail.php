<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">

<!--            --><?php //var_dump($model); var_dump($info); exit; ?>
            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'action' => 'web/site/signup']); ?>
<!--            echo $form->field($model, 'hidden1')->hiddenInput(['value'=> $value])->label(false);-->

            <?= $form->field($model, 'first_name')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'middle_name')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'last_name')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'birthday')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'gender')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'password')->hiddenInput()->label(false); ?>

            <div class="form-group">
                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
