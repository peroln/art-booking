<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <form role="form" enctype="multipart/form-data" action="<?php echo Url::to(['default/index']); ?>"
          method="post">

        <?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

        <div class="form-group">
            <label for="file1">File</label>
<!--            <input  multiple="multiple" name="file[]" type="file" id="file">-->
            <input name="file1[]" type="file" id="file1" multiple="multiple" accept="image/*,image/jpeg,img,png,gif">
            <input name="file2" type="file" id="file2">
        </div>

        <button type="submit" class="button">Submit</button>
    </form>

    <code><?= __FILE__ ?></code>
</div>
